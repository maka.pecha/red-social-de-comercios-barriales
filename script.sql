USE [master]
GO
/****** Object:  Database [RedSocial]    Script Date: 20/11/2020 01:00:27 p. m. ******/
CREATE DATABASE [RedSocial]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RedSocial', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\RedSocial.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'RedSocial_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\RedSocial_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [RedSocial] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RedSocial].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RedSocial] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RedSocial] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RedSocial] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RedSocial] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RedSocial] SET ARITHABORT OFF 
GO
ALTER DATABASE [RedSocial] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RedSocial] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RedSocial] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RedSocial] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RedSocial] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RedSocial] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RedSocial] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RedSocial] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RedSocial] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RedSocial] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RedSocial] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RedSocial] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RedSocial] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RedSocial] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RedSocial] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RedSocial] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RedSocial] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RedSocial] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RedSocial] SET  MULTI_USER 
GO
ALTER DATABASE [RedSocial] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RedSocial] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RedSocial] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RedSocial] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [RedSocial] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [RedSocial] SET QUERY_STORE = OFF
GO
USE [RedSocial]
GO
/****** Object:  UserDefinedFunction [dbo].[obtenerComentarioPorId]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[obtenerComentarioPorId] (@idComentario int)
		RETURNS @tabla table(id int,
							texto nvarchar(100),
							puntaje int,
							vecino nvarchar(30),
							comercio nvarchar(50),
							fecha nvarchar(10),
							respuesta nvarchar(10),
							activo bit
							)
		AS
		BEGIN
				INSERT INTO @tabla SELECT comen.id, comen.texto, comen.puntaje, comen.vecino, comer.nombre as comercio, comen.fecha, r.texto as respuesta, comen.activo
				FROM Comentarios comen JOIN 
				Comercios comer on comen.id_comercio = comer.id left join
				Respuestas r on comen.id = r.id_comentario
				WHERE comen.id = @idComentario
				return 
		END
GO
/****** Object:  UserDefinedFunction [dbo].[obtenerComentariosPorIdComercio]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[obtenerComentariosPorIdComercio] (@idComercio int)
		RETURNS @tabla table(id int,
							texto nvarchar(100),
							puntaje int,
							vecino nvarchar(30),
							comercio nvarchar(50),
							fecha nvarchar(10),
							respuesta nvarchar(100),
							activo bit
							)
		AS
		BEGIN
				INSERT INTO @tabla SELECT comen.id, comen.texto, comen.puntaje, comen.vecino, comer.nombre as comercio, comen.fecha, r.texto as respuesta, comen.activo
				FROM Comentarios comen inner JOIN 
				Comercios comer on comen.id_comercio = comer.id left join
				Respuestas r on comen.id = r.id_comentario
				WHERE comer.id = @idComercio
				AND comen.activo = 1
				return 
		END
GO
/****** Object:  UserDefinedFunction [dbo].[obtenerComercioPorId]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[obtenerComercioPorId] (@idComercio int)
		RETURNS @tabla table(id int,
							nombre varchar(50),
							descripcion varchar(50),
							imagen varchar(50),
							id_rubro int,
							activo bit
							)
		AS
		BEGIN
				INSERT INTO @tabla SELECT id, nombre, descripcion, imagen, id_rubro, activo
				FROM Comercios 
				WHERE id = @idComercio
				return 
		END
GO
/****** Object:  UserDefinedFunction [dbo].[obtenerOfertaPorId]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[obtenerOfertaPorId] (@idOferta int)
		RETURNS @tabla table(id int,
							titulo nvarchar(50),
							descripcion nvarchar(50),
							imagen nvarchar(50),
							precio_real real,
							precio_descuento real,
							id_comercio int,
							fecha nvarchar(10),
							activo bit
							)
		AS
		BEGIN
				INSERT INTO @tabla SELECT id, titulo, descripcion, imagen, precio_real, precio_descuento, id_comercio, fecha, activo
				FROM Ofertas 
				WHERE id = @idOferta
				return 
		END
GO
/****** Object:  UserDefinedFunction [dbo].[obtenerOfertasPorBusqueda]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[obtenerOfertasPorBusqueda] (@descripcion nvarchar(50))
		RETURNS @tabla table(id int,
							titulo nvarchar(50),
							descripcion nvarchar(50),
							imagen nvarchar(50),
							precio_real real,
							precio_descuento real,
							comercio nvarchar(50),
							fecha nvarchar(10),
							activo bit
							)
		AS
		BEGIN
				INSERT INTO @tabla SELECT o.id, o.titulo, o.descripcion, o.imagen, o.precio_real, o.precio_descuento, c.nombre as comercio, o.fecha, o.activo
				FROM Ofertas o join Comercios c on o.id_comercio = c.id
				WHERE o.descripcion like '%'+@descripcion+'%'
				AND o.activo=1
				return 
		END
GO
/****** Object:  UserDefinedFunction [dbo].[obtenerOfertasPorComercio]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[obtenerOfertasPorComercio] (@idComercio int)
		RETURNS @tabla table(id int,
							titulo nvarchar(50),
							descripcion nvarchar(50),
							imagen nvarchar(50),
							precio_real real,
							precio_descuento real,
							comercio nvarchar(50),
							fecha nvarchar(10),
							activo bit
							)
		AS
		BEGIN
				INSERT INTO @tabla SELECT o.id, o.titulo, o.descripcion, o.imagen, o.precio_real, o.precio_descuento, c.nombre, o.fecha, o.activo
				FROM Ofertas o join Comercios c on o.id_comercio = c.id
				WHERE id_comercio = @idComercio
				AND o.activo=1
				return 
		END
GO
/****** Object:  UserDefinedFunction [dbo].[obtenerRespuestaPorIdComentario]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[obtenerRespuestaPorIdComentario] (@idComentario int)
		RETURNS @tabla table(id int,
							texto nvarchar(100),
							id_comentario int,
							fecha nvarchar(10),
							activo bit
							)
		AS
		BEGIN
				INSERT INTO @tabla SELECT id, texto, id_comentario, fecha, activo
				FROM Respuestas
				WHERE id_comentario = @idComentario
				return 
		END
GO
/****** Object:  UserDefinedFunction [dbo].[obtenerRubroPorId]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[obtenerRubroPorId] (@idRubro int)
		RETURNS @tabla table(id int,
							nombre varchar(50),
							descripcion varchar(50),
							activo bit
							)
		AS
		BEGIN
				INSERT INTO @tabla SELECT id, nombre, descripcion, activo
				FROM Rubros 
				WHERE id = @idRubro
				return 
		END
GO
/****** Object:  UserDefinedFunction [dbo].[obtenerUsuarioPorId]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[obtenerUsuarioPorId] (@idUsuario int)
		RETURNS @tabla table(id int,
							nombre varchar(30),
							contraseña varchar(30),
							id_comercio int,
							activo bit
							)
		AS
		BEGIN
				INSERT INTO @tabla SELECT id, nombre, contraseña, id_comercio, activo
				FROM Usuarios 
				WHERE id = @idUsuario
				return 
		END
GO
/****** Object:  Table [dbo].[Comercios]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comercios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
	[descripcion] [nvarchar](50) NULL,
	[imagen] [nvarchar](50) NULL,
	[id_rubro] [int] NOT NULL,
	[activo] [int] NOT NULL,
 CONSTRAINT [PK_Comercios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](30) NOT NULL,
	[contraseña] [nvarchar](30) NOT NULL,
	[id_comercio] [int] NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[obtenerUsuariosComercio]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[obtenerUsuariosComercio]
AS
SELECT        dbo.Usuarios.id, dbo.Usuarios.nombre, dbo.Usuarios.contraseña, dbo.Comercios.nombre AS comercio, dbo.Usuarios.activo
FROM            dbo.Comercios RIGHT OUTER JOIN
                         dbo.Usuarios ON dbo.Comercios.id = dbo.Usuarios.id_comercio
WHERE        (dbo.Usuarios.activo = 1)
GO
/****** Object:  Table [dbo].[Rubros]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rubros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
	[descripcion] [nvarchar](50) NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_Rubros] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[obtenerComercios]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[obtenerComercios]
AS
SELECT        dbo.Comercios.id, dbo.Comercios.nombre, dbo.Comercios.descripcion, dbo.Comercios.imagen, dbo.Rubros.nombre AS rubro, dbo.Comercios.activo
FROM            dbo.Comercios INNER JOIN
                         dbo.Rubros ON dbo.Comercios.id_rubro = dbo.Rubros.id
WHERE        (dbo.Comercios.activo = 1)
GO
/****** Object:  Table [dbo].[Comentarios]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comentarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[texto] [nvarchar](100) NOT NULL,
	[puntaje] [int] NULL,
	[vecino] [nvarchar](30) NOT NULL,
	[id_comercio] [int] NOT NULL,
	[fecha] [nvarchar](10) NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_Comentarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Respuestas]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Respuestas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[texto] [nvarchar](100) NOT NULL,
	[id_comentario] [int] NOT NULL,
	[fecha] [nvarchar](10) NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_Respuestas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[obtenerComentarios]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[obtenerComentarios]
AS
SELECT        dbo.Comentarios.id, dbo.Comentarios.texto, dbo.Comentarios.puntaje, dbo.Comentarios.vecino, dbo.Comercios.nombre AS comercio, dbo.Comentarios.fecha, dbo.Respuestas.texto as respuesta, dbo.Comentarios.activo
FROM            dbo.Comentarios INNER JOIN
                dbo.Comercios ON dbo.Comentarios.id_comercio = dbo.Comercios.id LEFT JOIN
				dbo.Respuestas ON dbo.Comentarios.id = dbo.Respuestas.id_comentario
WHERE        (dbo.Comentarios.activo = 1)
GO
/****** Object:  View [dbo].[reporteCantidadComentariosPorComercio]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[reporteCantidadComentariosPorComercio]
AS
	select comer.nombre as comercio, count(comen.texto) as cantidadComentarios
	from obtenerComercios comer left join
	obtenerComentarios comen on comen.comercio = comer.nombre
	group by comer.nombre
GO
/****** Object:  View [dbo].[reportePromedioValoracionPorComercio]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[reportePromedioValoracionPorComercio]
AS
	select comer.nombre as comercio, avg(cast(comen.puntaje as float)) promedio
	from obtenerComercios comer left join
	obtenerComentarios comen on comen.comercio = comer.nombre
	group by comer.nombre
GO
/****** Object:  View [dbo].[reporteCantidadValoracionesPorEstrella]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[reporteCantidadValoracionesPorEstrella]
AS
	select comen.puntaje as estrellas, count(*) as cantidadValoraciones
	from obtenerComercios comer join
	obtenerComentarios comen on comen.comercio = comer.nombre
	group by comen.puntaje
GO
/****** Object:  View [dbo].[obtenerUsuarios]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[obtenerUsuarios]
AS
SELECT        id, nombre, contraseña, activo
FROM            dbo.Usuarios
WHERE        (activo = 1)
GO
/****** Object:  View [dbo].[obtenerRubros]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[obtenerRubros]
AS
SELECT        id, nombre, descripcion, activo
FROM            dbo.Rubros
WHERE        (activo = 1)
GO
/****** Object:  Table [dbo].[Ofertas]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ofertas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [nvarchar](50) NOT NULL,
	[descripcion] [nvarchar](50) NULL,
	[imagen] [nvarchar](50) NULL,
	[precio_real] [real] NOT NULL,
	[precio_descuento] [real] NOT NULL,
	[id_comercio] [int] NOT NULL,
	[fecha] [nvarchar](10) NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_Ofertas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[obtenerOfertas]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[obtenerOfertas]
AS
SELECT        TOP (100) PERCENT dbo.Ofertas.id, dbo.Ofertas.titulo, dbo.Ofertas.descripcion, dbo.Ofertas.imagen, dbo.Ofertas.precio_real, dbo.Ofertas.precio_descuento, dbo.Ofertas.fecha, dbo.Comercios.nombre AS comercio, 
                         dbo.Ofertas.activo
FROM            dbo.Comercios INNER JOIN
                         dbo.Ofertas ON dbo.Comercios.id = dbo.Ofertas.id_comercio
WHERE        (dbo.Ofertas.activo = 1)
ORDER BY CAST(dbo.Ofertas.fecha AS datetime) desc
GO
/****** Object:  View [dbo].[obtenerRespuestas]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[obtenerRespuestas]
AS
SELECT        id, texto, id_comentario, fecha
FROM            dbo.Respuestas
WHERE        (activo = 1)
GO
SET IDENTITY_INSERT [dbo].[Comentarios] ON 

INSERT [dbo].[Comentarios] ([id], [texto], [puntaje], [vecino], [id_comercio], [fecha], [activo]) VALUES (24, N'Me gusto mucho ese locro', 5, N'Maka', 1, N'19/11/2020', 1)
INSERT [dbo].[Comentarios] ([id], [texto], [puntaje], [vecino], [id_comercio], [fecha], [activo]) VALUES (25, N'Probando un nuevo mensaje', 4, N'Alan', 1, N'19/11/2020', 1)
INSERT [dbo].[Comentarios] ([id], [texto], [puntaje], [vecino], [id_comercio], [fecha], [activo]) VALUES (26, N'probando mensaje nuevo otra vez', 2, N'Zeus', 1, N'19/11/2020', 0)
INSERT [dbo].[Comentarios] ([id], [texto], [puntaje], [vecino], [id_comercio], [fecha], [activo]) VALUES (27, N'me gusta', 4, N'Daniel', 2, N'19/11/2020', 1)
INSERT [dbo].[Comentarios] ([id], [texto], [puntaje], [vecino], [id_comercio], [fecha], [activo]) VALUES (28, N'podria mejorar', 2, N'Raul', 2, N'19/11/2020', 1)
INSERT [dbo].[Comentarios] ([id], [texto], [puntaje], [vecino], [id_comercio], [fecha], [activo]) VALUES (29, N'No venden los productos esperados', 1, N'Andrea', 7, N'19/11/2020', 1)
INSERT [dbo].[Comentarios] ([id], [texto], [puntaje], [vecino], [id_comercio], [fecha], [activo]) VALUES (30, N'otro comentario para los reportes', 4, N'Victor', 1, N'19/11/2020', 1)
INSERT [dbo].[Comentarios] ([id], [texto], [puntaje], [vecino], [id_comercio], [fecha], [activo]) VALUES (31, N'Hola soy Gabi', 4, N'Gabi', 1, N'19/11/2020', 1)
INSERT [dbo].[Comentarios] ([id], [texto], [puntaje], [vecino], [id_comercio], [fecha], [activo]) VALUES (32, N'tu mami', 4, N'Pablo', 1, N'20/11/2020', 1)
SET IDENTITY_INSERT [dbo].[Comentarios] OFF
SET IDENTITY_INSERT [dbo].[Comercios] ON 

INSERT [dbo].[Comercios] ([id], [nombre], [descripcion], [imagen], [id_rubro], [activo]) VALUES (1, N'Villalpando', N'Comidas Regionales', N'villalpando.jpg', 1, 1)
INSERT [dbo].[Comercios] ([id], [nombre], [descripcion], [imagen], [id_rubro], [activo]) VALUES (2, N'Lapana', N'Panaderia', N'lapana.jpg', 4, 1)
INSERT [dbo].[Comercios] ([id], [nombre], [descripcion], [imagen], [id_rubro], [activo]) VALUES (5, N'Grido', N'Helados', N'grido.jpg', 1, 0)
INSERT [dbo].[Comercios] ([id], [nombre], [descripcion], [imagen], [id_rubro], [activo]) VALUES (7, N'Grido', N'Helados', N'grido.jpg', 1, 1)
INSERT [dbo].[Comercios] ([id], [nombre], [descripcion], [imagen], [id_rubro], [activo]) VALUES (8, N'Farmacity', N'Farmacia de barrio', N'farmacity.jpg', 2, 1)
INSERT [dbo].[Comercios] ([id], [nombre], [descripcion], [imagen], [id_rubro], [activo]) VALUES (13, N'San Honorato', N'restaurante', N'sanhonorato.jpg', 1, 1)
SET IDENTITY_INSERT [dbo].[Comercios] OFF
SET IDENTITY_INSERT [dbo].[Ofertas] ON 

INSERT [dbo].[Ofertas] ([id], [titulo], [descripcion], [imagen], [precio_real], [precio_descuento], [id_comercio], [fecha], [activo]) VALUES (5, N'Locro', N'Una oferta nueva de locro', N'locro.jpg', 30, 25, 1, N'19/11/2020', 1)
INSERT [dbo].[Ofertas] ([id], [titulo], [descripcion], [imagen], [precio_real], [precio_descuento], [id_comercio], [fecha], [activo]) VALUES (7, N'Otra', N'Oferta', NULL, 100, 80, 2, N'09/11/2020', 0)
INSERT [dbo].[Ofertas] ([id], [titulo], [descripcion], [imagen], [precio_real], [precio_descuento], [id_comercio], [fecha], [activo]) VALUES (8, N'prueba', N'prueba', N'cosmetica.png', 45, 23, 2, N'09/11/2020', 1)
INSERT [dbo].[Ofertas] ([id], [titulo], [descripcion], [imagen], [precio_real], [precio_descuento], [id_comercio], [fecha], [activo]) VALUES (9, N'prueba 2', N'michiiiiiiiiiiiiii', N'desodorante.png', 56, 23, 7, N'09/11/2020', 1)
INSERT [dbo].[Ofertas] ([id], [titulo], [descripcion], [imagen], [precio_real], [precio_descuento], [id_comercio], [fecha], [activo]) VALUES (10, N'oferta', N'CarnicerÃ­a barrial', N'caudalie.jpg', 68, 56, 7, N'09/11/2020', 1)
INSERT [dbo].[Ofertas] ([id], [titulo], [descripcion], [imagen], [precio_real], [precio_descuento], [id_comercio], [fecha], [activo]) VALUES (11, N'Humita', N'', N'humita.jpg', 300, 250, 1, N'19/11/2020', 1)
SET IDENTITY_INSERT [dbo].[Ofertas] OFF
SET IDENTITY_INSERT [dbo].[Respuestas] ON 

INSERT [dbo].[Respuestas] ([id], [texto], [id_comentario], [fecha], [activo]) VALUES (1, N'Genial!', 24, N'19/11/2020', 1)
INSERT [dbo].[Respuestas] ([id], [texto], [id_comentario], [fecha], [activo]) VALUES (3, N'Miau', 25, N'19/11/2020', 1)
INSERT [dbo].[Respuestas] ([id], [texto], [id_comentario], [fecha], [activo]) VALUES (4, N'te respondo', 26, N'19/11/2020', 1)
INSERT [dbo].[Respuestas] ([id], [texto], [id_comentario], [fecha], [activo]) VALUES (5, N'bueno', 30, N'20/11/2020', 1)
SET IDENTITY_INSERT [dbo].[Respuestas] OFF
SET IDENTITY_INSERT [dbo].[Rubros] ON 

INSERT [dbo].[Rubros] ([id], [nombre], [descripcion], [activo]) VALUES (1, N'Gastronomia', N'Gastronomia barrial', 1)
INSERT [dbo].[Rubros] ([id], [nombre], [descripcion], [activo]) VALUES (2, N'Cuidado Personal', N'Cuidado Personal barrial', 1)
INSERT [dbo].[Rubros] ([id], [nombre], [descripcion], [activo]) VALUES (3, N'Almacén', N'Almacén barrial', 1)
INSERT [dbo].[Rubros] ([id], [nombre], [descripcion], [activo]) VALUES (4, N'Panaderia', N'Panaderia barrial ', 1)
INSERT [dbo].[Rubros] ([id], [nombre], [descripcion], [activo]) VALUES (5, N'Carnicería', N'Carnicería barrial', 0)
INSERT [dbo].[Rubros] ([id], [nombre], [descripcion], [activo]) VALUES (6, N'Carniceria', N'Carniceria barrial', 0)
INSERT [dbo].[Rubros] ([id], [nombre], [descripcion], [activo]) VALUES (7, N'Carniceria', N'carniceria de barrio', 1)
SET IDENTITY_INSERT [dbo].[Rubros] OFF
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([id], [nombre], [contraseña], [id_comercio], [activo]) VALUES (2, N'maka', N'123', NULL, 1)
INSERT [dbo].[Usuarios] ([id], [nombre], [contraseña], [id_comercio], [activo]) VALUES (6, N'alan', N'123', 1, 1)
INSERT [dbo].[Usuarios] ([id], [nombre], [contraseña], [id_comercio], [activo]) VALUES (7, N'prueba', N'maka123', 2, 0)
INSERT [dbo].[Usuarios] ([id], [nombre], [contraseña], [id_comercio], [activo]) VALUES (11, N'tu mami', N'maka123', 1, 0)
INSERT [dbo].[Usuarios] ([id], [nombre], [contraseña], [id_comercio], [activo]) VALUES (12, N'Pablo', N'pablo123', 1, 1)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UC_nombre]    Script Date: 20/11/2020 01:00:28 p. m. ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [UC_nombre] UNIQUE NONCLUSTERED 
(
	[nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Comentarios]  WITH CHECK ADD  CONSTRAINT [FK_Comentarios_Comercios] FOREIGN KEY([id_comercio])
REFERENCES [dbo].[Comercios] ([id])
GO
ALTER TABLE [dbo].[Comentarios] CHECK CONSTRAINT [FK_Comentarios_Comercios]
GO
ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_Rubros] FOREIGN KEY([id_rubro])
REFERENCES [dbo].[Rubros] ([id])
GO
ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_Rubros]
GO
ALTER TABLE [dbo].[Ofertas]  WITH CHECK ADD  CONSTRAINT [FK_Ofertas_Comercios] FOREIGN KEY([id_comercio])
REFERENCES [dbo].[Comercios] ([id])
GO
ALTER TABLE [dbo].[Ofertas] CHECK CONSTRAINT [FK_Ofertas_Comercios]
GO
ALTER TABLE [dbo].[Respuestas]  WITH CHECK ADD  CONSTRAINT [FK_Respuestas_Comentarios] FOREIGN KEY([id_comentario])
REFERENCES [dbo].[Comentarios] ([id])
GO
ALTER TABLE [dbo].[Respuestas] CHECK CONSTRAINT [FK_Respuestas_Comentarios]
GO
/****** Object:  StoredProcedure [dbo].[altaComentario]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[altaComentario] (
	@texto nvarchar(100), 
	@puntaje int,
	@vecino nvarchar(30),
	@id_comercio int,
	@fecha nvarchar(10),
	@activo bit
	)
	AS
	BEGIN
		INSERT INTO Comentarios(texto, puntaje, vecino, id_comercio, fecha, activo)
		VALUES(@texto,@puntaje,@vecino,@id_comercio,@fecha,@activo)
	END 
GO
/****** Object:  StoredProcedure [dbo].[altaComercio]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[altaComercio] (
	@nombre nvarchar(50), 
	@descripcion nvarchar(50),
	@imagen nvarchar(50),
	@id_rubro int,
	@activo bit
	)
	AS
	BEGIN
		INSERT INTO Comercios (nombre, descripcion, imagen, id_rubro, activo)
		VALUES(@nombre,@descripcion,@imagen,@id_rubro,@activo)
	END 
GO
/****** Object:  StoredProcedure [dbo].[altaOferta]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[altaOferta] (
	@titulo nvarchar(50), 
	@descripcion nvarchar(50),
	@imagen nvarchar(50),
	@precio_real real,
	@precio_descuento real,
	@id_comercio int,
	@fecha nvarchar(10),
	@activo bit
	)
	AS
	BEGIN
		INSERT INTO Ofertas (titulo, descripcion, imagen, precio_real, precio_descuento, id_comercio, fecha, activo)
		VALUES(@titulo,@descripcion,@imagen,@precio_real,@precio_descuento,@id_comercio,@fecha,@activo)
	END 
GO
/****** Object:  StoredProcedure [dbo].[altaRespuesta]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[altaRespuesta] (
	@texto nvarchar(100), 
	@id_comentario int,
	@fecha nvarchar(10),
	@activo bit
	)
	AS
	BEGIN
		INSERT INTO Respuestas(texto, id_comentario, fecha, activo)
		VALUES(@texto,@id_comentario,@fecha,@activo)
	END 
GO
/****** Object:  StoredProcedure [dbo].[altaRubro]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[altaRubro] (
	@nombre nvarchar(50), 
	@descripcion nvarchar(50),
	@activo bit
	)
	AS
	BEGIN
		INSERT INTO Rubros (nombre, descripcion, activo)
		VALUES(@nombre,@descripcion,@activo)
	END 
GO
/****** Object:  StoredProcedure [dbo].[altaUsuario]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[altaUsuario] (
	@nombre nvarchar(30), 
	@contraseña nvarchar(30),
	@id_comercio int,
	@activo bit
	)
	AS
	BEGIN
		INSERT INTO Usuarios (nombre, contraseña, id_comercio, activo)
		VALUES(@nombre,@contraseña,@id_comercio,@activo)
	END 
GO
/****** Object:  StoredProcedure [dbo].[bajaComentario]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[bajaComentario] (
	@id int
	)
	AS
	BEGIN
		UPDATE Comentarios SET activo = 0  WHERE id = @id
	END
GO
/****** Object:  StoredProcedure [dbo].[bajaComercio]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[bajaComercio] (
	@id int
	)
	AS
	BEGIN
		UPDATE Comercios SET activo = 0  WHERE id = @id
	END
GO
/****** Object:  StoredProcedure [dbo].[bajaOferta]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[bajaOferta] (
	@id int
	)
	AS
	BEGIN
		UPDATE Ofertas SET activo = 0  WHERE id = @id
	END
GO
/****** Object:  StoredProcedure [dbo].[bajaRespuesta]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[bajaRespuesta] (
	@id int
	)
	AS
	BEGIN
		UPDATE Respuestas SET activo = 0  WHERE id = @id
	END
GO
/****** Object:  StoredProcedure [dbo].[bajaRubro]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[bajaRubro] (
	@id int
	)
	AS
	BEGIN
		UPDATE Rubros SET activo = 0  WHERE id = @id
	END
GO
/****** Object:  StoredProcedure [dbo].[bajaUsuario]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[bajaUsuario] (
	@id int
	)
	AS
	BEGIN
		UPDATE Usuarios SET activo = 0  WHERE id = @id
	END
GO
/****** Object:  StoredProcedure [dbo].[modificarComentario]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[modificarComentario](
		@texto NVARCHAR(100),
		@puntaje int,
		@vecino NVARCHAR(30),
		@idComercio int,
		@fecha nvarchar(10),
		@idComentario int
		) AS
		BEGIN
			UPDATE Comentarios
			SET texto = @texto, 
				puntaje = @puntaje, 
				vecino = @vecino, 
				id_comercio = @idComercio, 
				fecha = @fecha
			WHERE id = @idComentario
		END
GO
/****** Object:  StoredProcedure [dbo].[modificarComercio]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[modificarComercio](
		@nombre NVARCHAR(50),
		@descripcion NVARCHAR(50),
		@imagen NVARCHAR(50),
		@idRubro int,
		@activo bit,
		@idComercio int
		) AS
		BEGIN
			UPDATE Comercios
			SET nombre = @nombre, descripcion = @descripcion, imagen = @imagen, id_rubro = @idRubro, activo = @activo
			WHERE id = @idComercio
		END
GO
/****** Object:  StoredProcedure [dbo].[modificarOferta]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[modificarOferta](
		@titulo NVARCHAR(50),
		@descripcion NVARCHAR(50),
		@imagen NVARCHAR(50),
		@precio_real real,
		@precio_descuento real,
		@idComercio int,
		@fecha NVARCHAR(10),
		@activo bit,
		@idOferta int
		) AS
		BEGIN
			UPDATE Ofertas
			SET titulo = @titulo, 
				descripcion = @descripcion, 
				imagen = @imagen, 
				precio_real = @precio_real, 
				precio_descuento = @precio_descuento, 
				id_comercio = @idComercio, 
				fecha = @fecha,
				activo = @activo
			WHERE id = @idOferta
		END
GO
/****** Object:  StoredProcedure [dbo].[modificarRespuesta]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[modificarRespuesta](
		@texto NVARCHAR(100),
		@idComentario int,
		@fecha nvarchar(10),
		@idRespuesta int
		) AS
		BEGIN
			UPDATE Respuestas
			SET texto = @texto, 
				id_comentario = @idComentario, 
				fecha = @fecha
			WHERE id = @idRespuesta
		END
GO
/****** Object:  StoredProcedure [dbo].[modificarRubro]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[modificarRubro] (
		@nombre NVARCHAR(50),
		@descripcion NVARCHAR(50),
		@idRubro int
		) AS
		BEGIN
			UPDATE Rubros
			SET nombre = @nombre, descripcion = @descripcion
			WHERE id = @idRubro
		END
GO
/****** Object:  StoredProcedure [dbo].[modificarUsuario]    Script Date: 20/11/2020 01:00:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[modificarUsuario](
		@nombre NVARCHAR(30),
		@contraseña NVARCHAR(30),
		@idComercio int,
		@idUsuario int
		) AS
		BEGIN
			UPDATE Usuarios
			SET nombre = @nombre, 
				contraseña = @contraseña, 
				id_comercio = @idComercio
			WHERE id = @idUsuario
		END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[35] 4[27] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Comentarios"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "Comercios"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 2
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1455
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerComentarios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerComentarios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[27] 4[34] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Comercios"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Rubros"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerComercios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerComercios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[32] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Comercios"
            Begin Extent = 
               Top = 15
               Left = 227
               Bottom = 145
               Right = 397
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Ofertas"
            Begin Extent = 
               Top = 6
               Left = 0
               Bottom = 136
               Right = 182
            End
            DisplayFlags = 280
            TopColumn = 5
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1605
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerOfertas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerOfertas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Respuestas"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerRespuestas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerRespuestas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Rubros"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerRubros'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerRubros'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Usuarios"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1380
         Table = 1170
         Output = 1575
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerUsuarios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerUsuarios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Comercios"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Usuarios"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerUsuariosComercio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'obtenerUsuariosComercio'
GO
USE [master]
GO
ALTER DATABASE [RedSocial] SET  READ_WRITE 
GO
