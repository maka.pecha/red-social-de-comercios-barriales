<%-- 
    Document   : menu
    Created on : 9 nov. 2020, 12:59:10
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <header>
        <nav id="nav" class="navbar nav-transparent">
            <div class="container">
                <div class="navbar-header">
                    <div class="navbar-brand">
                        <a href="https://www.frc.utn.edu.ar/"> 
                            <img class="logo" src="Assets/Images/front/logoutn.png" alt="logo">
                            <img class="logo-alt" src="Assets/Images/front/logoutn2.png" alt="logo">
                        </a>
                    </div>
                    <div class="nav-collapse">
                        <span></span>
                    </div>
                 </div>
                
                <c:if test="${usuario != null}">
                    <ul class="main-nav nav navbar-nav navbar-right tamañoMenu">
                    <li class="active"><a href="index.jsp">Home</a></li>
                    <li class="has-dropdown"><a>Usuarios</a>
                        <ul class="dropdown">
                            <li><a href="/RedSocialVecinal/ListadoUsuarios">Listado de Usuarios</a></li>
                            <li><a href="/RedSocialVecinal/AgregarUsuario">Alta de Usuarios</a></li>
                        </ul>
                    </li>
                    <li class="has-dropdown"><a>Ofertas</a>
                        <ul class="dropdown">
                            <li><a href="/RedSocialVecinal/ListadoOfertas">Listado de Ofertas</a></li>
                            <li><a href="/RedSocialVecinal/AgregarOferta">Alta de Ofertas</a></li>
                        </ul>
                    </li>
                    <li class="has-dropdown"><a>Rubros</a>
                        <ul class="dropdown">
                            <li><a href="/RedSocialVecinal/ListadoRubros">Listado de Rubros</a></li>
                            <li><a href="/RedSocialVecinal/AgregarRubro">Alta de Rubros</a></li>
                        </ul>
                    </li>
                    <li class="has-dropdown"><a>Comercios</a>
                        <ul class="dropdown">
                            <li><a href="/RedSocialVecinal/ListadoComercios">Listado de Comercios</a></li>
                            <li><a href="/RedSocialVecinal/AgregarComercio">Alta de Comercios</a></li>
                        </ul>
                    </li>
                    <li><a href="/RedSocialVecinal/Reportes">Reportes</a></li>
                    <li><a href="/RedSocialVecinal/Login?accion=cerrar">Cerrar Sesión</a></li>
                </ul>
                </c:if>
            </div>
        </nav>
    </header>
</html>
