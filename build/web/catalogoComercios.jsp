<%-- 
    Document   : catalogoOfertas
    Created on : 9 nov. 2020, 13:23:32
    Author     : Maka
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Catálogo Comercios</title>
    </head>
  
  <body style="">
    <%@include file="menuVisitas.jsp" %>

<!-------------------------------------------HOME---------------------------------------------------->   
<div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal">

                <div class="secundarioCliente table-wrapper-scroll-y my-custom-scrollbar2">
                    <h1 style="padding-left: 5%">Conozca nuestros Comercios</h1>
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">NOMBRE</th>
                            <th scope="col">DESCRIPCION</th>
                            <th scope="col">IMAGEN</th>
                            <th scope="col">RUBRO</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listaCa}" var="c">      
                                <tr>
                                  <th scope="row">${c.getId()}</th>
                                  <td>${c.getNombre()}</td>
                                  <td>${c.getDescripcion()}</td>
                                  <td><img class="img-thumbnail" style="width:150px; height:auto;" src="Assets/Images/${c.getImagen()}"></td>
                                  <td>${c.getRubro()}</td>
                                  <td><a href="/RedSocialVecinal/CatalogoOfertasPorComercio?id=${c.getId()}&name=${c.getNombre()}"><button type="button" class="btn btn-info">OFERTAS</button></a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>

<!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>

