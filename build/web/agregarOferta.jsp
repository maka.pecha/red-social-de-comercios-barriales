<%-- 
    Document   : agregarOferta
    Created on : 9 nov. 2020, 0:58:39
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
   <head>
            <%@include file="head.jsp" %>
            <title>Agregar Oferta</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>

<!-------------------------------------------HOME---------------------------------------------------->      
  <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal table-wrapper-scroll-y my-custom-scrollbar2">

                <div class="secundarioCliente ">
                    <a href="/RedSocialVecinal/ListadoOfertas" style="color: #fff" class="float-right"> Volver al Listado</a>
                    <h1 style="padding-left: 5%">OFERTA</h1>
        
        <form method="POST" action="/RedSocialVecinal/AgregarOferta" enctype="multipart/form-data" onsubmit="return validar()">
            <input hidden="" type="number" name="txtId" value="${id}" class="form-control" id="txtId">
            <div class="form-group">
              <label for="txtTitulo">TITULO</label>
              <input name="txtTitulo" type="text" id="txtTitulo" required value="${titulo}"  placeholder="Ingrese Título" class="form-control"/>
            </div>
            <div class="form-group">
              <label for="txtDescripcion">DESCRIPCION</label>
              <input type="text" name="txtDescripcion" value="${descripcion}" class="form-control" id="txtDescripcion" placeholder="Ingrese Descripción">
            </div>
            <div class="form-group">
                <label for="txtFoto">IMAGEN</label>
                <div class="custom-file was-validated" id="txtFoto" >   
                    <input type="file" name="txtFoto" value="${foto}" class="custom-file-input" id="validatedCustomFile" accept=".png, .jpg, .jpeg"/>
                    <label class="custom-file-label" for="validatedCustomFile">Elegir imagen...</label>
                    <div class="invalid-feedback">Debe elegir una foto</div>
                </div>
            </div>
            <div class="form-group">
                <label for="txtPrecio_real">PRECIO</label>
                <input type="number" name="txtPrecio_real" value="${precio_real}" class="form-control" id="txtPrecio_real" placeholder="Ingrese Precio" required>
              </div>
            <div class="form-group">
                <label for="txtPrecio_descuento">PRECIO DE DESCUENTO</label>
                <input type="number" name="txtPrecio_descuento" value="${precio_descuento}" class="form-control" id="txtPrecio_descuento" placeholder="Ingrese Precio de Descuento" required>
            </div>
            <div class="form-group">
              <label for="txtComercio">COMERCIO</label>
              <select name="txtComercio" id="txtComercio" class="btn btn-danger">
                  <c:forEach items="${listaC}" var="c">
                      <option value="${c.getId()}"<c:if test="${c.getId() == idComercioSelected}">selected</c:if>>${c.getNombre()}</option>
                </c:forEach>
              </select>
            </div>
            <button type="submit" onclick="validarAltaOferta()" class="btn btn-primary float-right">AGREGAR</button>
        </form>
                </div>
            </div>
    </div>
   
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>
