<%-- 
    Document   : newjspl
    Created on : 8 nov. 2020, 9:14:50
    Author     : Maka
--%>
<%
    HttpSession sesion = request.getSession();
    Object usuario = sesion.getAttribute("usuario");

    if (usuario == null) {
        response.sendRedirect("/RedSocialVecinal/Login?accion=cerrar");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Usuarios</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>
<!-------------------------------------------HOME---------------------------------------------------->      
        <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal ">

                <div class="secundarioCliente table-wrapper-scroll-y my-custom-scrollbar2">
                     <h1 style="padding-left: 5%">LISTADO DE USUARIOS</h1>
        <table class="table table-hover table-dark">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">NOMBRE</th>
                <th scope="col">COMERCIO</th>
              </tr>
            </thead>
            <tbody>
                <c:forEach items="${lista}" var="u">      
                    <tr>
                      <th scope="row">${u.getId()}</th>
                      <td>${u.getNombre()}</td>
                      <td>${u.getComercio()}</td>
                      <td><a href="/RedSocialVecinal/AgregarUsuario?id=${u.getId()}"><button type="button" class="btn btn-outline-info">Editar</button></a></td>
                      <td><a href="/RedSocialVecinal/EliminarUsuario?id=${u.getId()}"><button type="button" class="btn btn-outline-danger">Eliminar</button></a></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        
                        
                </div>
            </div>
    </div>
 <!-------------------------------------------FIN HOME------------------------------------------------>     
   <%@include file="footer.jsp" %>
</body>
</html>