<%-- 
    Document   : index
    Created on : 8 nov. 2020, 6:34:57
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Agregar Usuario</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>
 
<!-------------------------------------------HOME---------------------------------------------------->      
  <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal table-wrapper-scroll-y my-custom-scrollbar2">

                <div class="secundarioCliente ">
                    <a href="/RedSocialVecinal/ListadoUsuarios" style="color: #fff" class="float-right"> Volver al Listado</a>
                    <h1 style="padding-left: 5%">USUARIO</h1>
        
        <form method="POST" action="/RedSocialVecinal/AgregarUsuario" onsubmit="return validar()">
            <input hidden="" type="number" name="txtId" value="${id}" class="form-control" id="txtId">
            <div class="form-group">
              <label for="txtNombre">NOMBRE</label>
              <input name="txtNombre" type="text" id="txtNombre" required value="${nombre}"  placeholder="Ingrese Nombre" class="form-control"/>
            </div>
            <div class="form-group">
              <label for="txtPass">CONTRASEÑA</label>
              <input type="text" name="txtPass" value="${contraseña}" class="form-control" id="txtPass" placeholder="Ingrese Contraseña" required>
            </div>
             <div class="form-group">
              <label for="txtComercio">COMERCIO</label>
              <select name="txtComercio" id="txtComercio" class="btn btn-danger">
                  <c:forEach items="${listaC}" var="c">
                      <option value="${c.getId()}"<c:if test="${c.getId() == idComercioSelected}">selected</c:if>>${c.getNombre()}</option>
                </c:forEach>
              </select>
            </div>
            <button type="submit" onclick="validarAltaUsuario()" class="btn btn-primary float-right">AGREGAR</button>
        </form>
                </div>
            </div>
    </div>
   
 <!-------------------------------------------FIN HOME------------------------------------------------>     
     <%@include file="footer.jsp" %><%@include file="footer.jsp" %>
</body>
</html>
