<%-- 
    Document   : catalogoOfertas
    Created on : 9 nov. 2020, 13:58:39
    Author     : Maka
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Catálogo Ofertas</title>
    </head>
  
  <body style="">
    <%@include file="menuVisitas.jsp" %>
<!-------------------------------------------HOME---------------------------------------------------->      
        <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal ">

                <div class="secundarioCliente table-wrapper-scroll-y my-custom-scrollbar2">
                    <a href="/RedSocialVecinal/CatalogoComercios"> <button class="btn btn-lg float-right" style="color: red">Volver a los Comercios</button></a>
                    <!--<a href="/RedSocialVecinal/CatalogoComercios" style="color: red" class="float-right"> Volver a Comercios</a>-->
                    <h1 style="padding-left: 5%">Ofertas de ${nombre_comercio}</h1> 
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">TITULO</th>
                            <th scope="col">DESCRIPCION</th>
                            <th scope="col">IMAGEN</th>
                            <th scope="col">PRECIO</th>
                            <th scope="col">DESC.</th>
                            <th scope="col">FECHA</th>
                            <th scope="col">COMERCIO</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listaO}" var="o">      
                                <tr>
                                  <th scope="row">${o.getId()}</th>
                                  <td>${o.getTitulo()}</td>
                                  <td>${o.getDescripcion()}</td>
                                  <td><img class="img-thumbnail" style="width:150px; height:auto;" src="Assets/Images/${o.getImagen()}"></td>
                                  <td>$ ${o.getPrecio_real()}</td>
                                  <td>$ ${o.getPrecio_descuento()}</td>
                                  <td>${o.getFecha()}</td>
                                  <td>${o.getComercio()}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <a type="button" class="btn btn-danger float-right" href="#txtMensaje" onClick="showAddComent()">Agregar un comentario</a>
                    <h4 style="padding-left: 5%; color: white">Comentarios</h4>
                    <c:if test="${listaCo == []}">
                        <h4 id="sinComentarios" style="padding-top: 5%; color: white; display: flex;justify-content: center;">Este comercio no tiene comentarios. Se el primero en dar tu opinión!</h2>
                    </c:if>
                    <c:if test="${listaCo != []}"> 
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">Mensaje</th>
                            <th scope="col">Estrellas</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Respuesta</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listaCo}" var="c">      
                                <tr>
                                  <td>${c.getTexto()}</td>
                                  <td>${c.getPuntaje()}</td>
                                  <td>${c.getVecino()}</td>
                                  <td>${c.getFecha()}</td>
                                  <td>${c.getRespuesta()}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    </c:if>
                        <div id="agregarComentario" style="display: none">
                            <%@include file="agregarComentario.jsp" %>
                        </div>
                </div>
            </div>
    </div>
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
    
</body>
<script>
    function showAddComent()
    {
        document.getElementById("agregarComentario").style.display="block";
        document.getElementById("sinComentarios").style.display="none";
        document.getElementById("txtMensaje").focus();
    }
</script>
</html>