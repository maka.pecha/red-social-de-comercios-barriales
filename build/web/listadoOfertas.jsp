<%-- 
    Document   : listadoOfertas
    Created on : 8 nov. 2020, 23:07:50
    Author     : Maka
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Ofertas</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>
<!-------------------------------------------HOME---------------------------------------------------->      
        <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal ">

                <div class="secundarioCliente table-wrapper-scroll-y my-custom-scrollbar2">
                    <h1 style="padding-left: 5%">LISTADO DE OFERTAS</h1>
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">TITULO</th>
                            <th scope="col">DESCRIPCION</th>
                            <th scope="col">IMAGEN</th>
                            <th scope="col">PRECIO</th>
                            <th scope="col">DESC.</th>
                            <th scope="col">FECHA</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listaO}" var="o">      
                                <tr>
                                  <th scope="row">${o.getId()}</th>
                                  <td>${o.getTitulo()}</td>
                                  <td>${o.getDescripcion()}</td>
                                  <td><img class="img-thumbnail" style="width:150px; height:auto;" src="Assets/Images/${o.getImagen()}"></td>
                                  <td>$ ${o.getPrecio_real()}</td>
                                  <td>$ ${o.getPrecio_descuento()}</td>
                                  <td>${o.getFecha()}</td>
                                  <td>${o.getComercio()}</td>
                                  <td><a href="/RedSocialVecinal/AgregarOferta?id=${o.getId()}"><button type="button" class="btn btn-outline-info">Editar</button></a></td>
                                  <td><a href="/RedSocialVecinal/EliminarOferta?id=${o.getId()}"><button type="button" class="btn btn-outline-danger">Eliminar</button></a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>
