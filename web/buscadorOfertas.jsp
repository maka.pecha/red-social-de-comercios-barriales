<%-- 
    Document   : buscadorOfertas
    Created on : 9 nov. 2020, 15:47:58
    Author     : Maka
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Catálogo Ofertas</title>
    </head>
  
  <body style="">
    <%@include file="menuVisitas.jsp" %>
<!-------------------------------------------HOME---------------------------------------------------->      
        <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal ">

                <div class="secundarioCliente table-wrapper-scroll-y my-custom-scrollbar2">
                    <a href="/RedSocialVecinal/BuscadorOfertas"> <button class="btn btn-lg float-right" style="color: red">Refrescar</button></a>
                    <!--<a href="/RedSocialVecinal/CatalogoComercios" style="color: red" class="float-right"> Volver a Comercios</a>-->
                    <h1 style="padding-left: 5%">Todas las Ofertas</h1> 
                    <form action="/RedSocialVecinal/BuscadorOfertas" method="POST">                
                        <div class="form-group">
                            <label>Ingrese su Búsqueda por titulo o descripción:</label>
                            <input name="txtDescripcion" class="form-control"/>                
                            <input type="submit" value="Buscar" class="btn btn-danger float-right btn-sm p-1 my-1 mx-1"/>
                        </div>
                    </form>
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">TITULO</th>
                            <th scope="col">DESCRIPCION</th>
                            <th scope="col">IMAGEN</th>
                            <th scope="col">PRECIO</th>
                            <th scope="col">DESC.</th>
                            <th scope="col">FECHA</th>
                            <th scope="col">COMERCIO</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listaO}" var="o">      
                                <tr>
                                  <th scope="row">${o.getId()}</th>
                                  <td>${o.getTitulo()}</td>
                                  <td>${o.getDescripcion()}</td>
                                  <td><img class="img-thumbnail" style="width:150px; height:auto;" src="Assets/Images/${o.getImagen()}"></td>
                                  <td>$ ${o.getPrecio_real()}</td>
                                  <td>$ ${o.getPrecio_descuento()}</td>
                                  <td>${o.getFecha()}</td>
                                  <td>${o.getComercio()}</td>
<!--                                  <td><a href="/RedSocialVecinal/AgregarOferta?id=${o.getId()}"><button type="button" class="btn btn-info">EDITAR</button></a></td>
                                  <td><a href="/RedSocialVecinal/EliminarOferta?id=${o.getId()}"><button type="button" class="btn btn-outline-danger">ELIMINAR</button></a></td>-->
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>