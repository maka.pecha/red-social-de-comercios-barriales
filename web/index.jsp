<%-- 
    Document   : index
    Created on : 8 nov. 2020, 6:34:57
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Home</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>

<!-------------------------------------------HOME---------------------------------------------------->      
    <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal">
                  <div class="container">
                        <div class="indx">
                            <div >
                                <div class="home-content">
                                    <img class="img-thumbnail" style="width:250px; height:auto;" src="Assets/Images/front/comercio.png">
                                    <h1 class="white-text">Vecinos Conectados</h1>
                                    <h3 class="white-text">Red Social de Comercios de Barrio</h3>
                                    <p class="white-text">
                                        Trabajo Práctico Final
                                    </p>
                                    <p class="white-text">
                                        Laboratorio IV 
                                    </p>
                                    <p class="white-text">
                                        PECHA, Janet Macarena
                                    </p>
                                    <c:if test="${usuario != null}">
                                        <h2>Bienvenido a la Administración del Sistema</h2>
                                    </c:if>
                                    <a href="/RedSocialVecinal/CatalogoComercios"> <button class="white-btn">Catálogo de Comercios y Ofertas</button></a>
                                     <c:if test="${usuario == null}">
                                        <a href="/RedSocialVecinal/login.jsp"> <button class="main-btn" >Iniciar Sesión</button></a>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
    </div>
   
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>
