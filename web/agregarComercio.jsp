<%-- 
    Document   : agregarComercio
    Created on : 8 nov. 2020, 20:53:09
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Agregar Comercio</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>

<!-------------------------------------------HOME---------------------------------------------------->      
  <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal table-wrapper-scroll-y my-custom-scrollbar2">

                <div class="secundarioCliente ">
                    <a href="/RedSocialVecinal/ListadoComercios" style="color: #fff" class="float-right"> Volver al Listado</a>
                    <h1 style="padding-left: 5%">COMERCIO</h1>
                    
        
        <form method="POST" action="/RedSocialVecinal/AgregarComercio" enctype="multipart/form-data" onsubmit="return validar()">
            <input hidden="" type="number" name="txtId" value="${id}" class="form-control" id="txtId">
            <div class="form-group">
              <label for="txtNombre">NOMBRE</label>
              <input name="txtNombre" type="text" id="txtNombre" required value="${nombre}"  placeholder="Ingrese Nombre" class="form-control"/>
            </div>
            <div class="form-group">
              <label for="txtDescripcion">DESCRIPCION</label>
              <input type="text" name="txtDescripcion" value="${descripcion}" class="form-control" id="txtDescripcion" placeholder="Ingrese Descripción">
            </div>
            <div class="form-group">
                <label for="txtFoto">IMAGEN</label>
                <div class="custom-file was-validated" id="txtFoto" >   
                    <input type="file" name="txtFoto" value="${foto}" class="custom-file-input" id="validatedCustomFile" accept=".png, .jpg, .jpeg"/>
                    <label class="custom-file-label" for="validatedCustomFile">Elegir imagen...</label>
                    <div class="invalid-feedback">Debe elegir una foto</div>
                </div>
            </div>
            <div class="form-group">
              <label for="txtRubro">RUBRO</label>
              <select name="txtRubro" id="txtRubro" class="btn btn-danger">
                  <c:forEach items="${listaRubros}" var="r">
                      <option value="${r.getId()}"<c:if test="${r.getId() == idRubroSelected}">selected</c:if>>${r.getNombre()}</option>
                </c:forEach>
              </select>
            </div>
                <button type="submit" onclick="validarAltaUsuario()" class="btn btn-primary float-right">AGREGAR</button>
        </form>
                </div>
            </div>
    </div>
   
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>
