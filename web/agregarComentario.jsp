<%-- 
    Document   : agregarComentario
    Created on : 10 nov. 2020, 17:30:50
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <body>
        <form  method="POST" action="/RedSocialVecinal/AgregarComentario">
            <input hidden="" type="number" name="txtId_comercio" value="${id_comercio}"  class="form-control" id="txtId_comercio">
            <input hidden="" type="text" name="txtNombre_comercio" value="${nombre_comercio}" class="form-control" id="txtNombre_comercio">
            <div class="form-group">
              <label for="txtMensaje">COMENTARIO NUEVO</label>
              <input name="txtMensaje" type="text" id="txtMensaje" required autofocus placeholder="Ingrese el Mensaje" class="form-control"/>
            </div>
            <div class="form-group">
              <label for="txtEstrellas">ESTRELLAS</label>
              <select name="txtEstrellas" id="txtEstrellas" class="btn btn-danger">
                  <c:forEach items="${listaEstrellas}" var="e">
                      <option value="${e.getNumero()}">${e.getTexto()}</option>
                </c:forEach>
              </select>
            </div>
            <div class="form-group">
              <label for="txtVecino">NOMBRE</label>
              <input type="text" name="txtVecino" class="form-control" id="txtVecino" placeholder="Ingrese su Nombre">
            </div>
            <input type="submit" class="btn btn-primary float-right"  style="margin: 0 10px" value="Agregar"></button>
            <a href="/RedSocialVecinal/CatalogoOfertasPorComercio?id=${id_comercio}&name=${nombre_comercio}"><button type="button" class="btn btn-danger float-right">Cancelar</button></a>
        </form>
    </body>
</html>
