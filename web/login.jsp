<%-- 
    Document   : login
    Created on : 8 nov. 2020, 7:16:31
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <%@include file="head.jsp" %>
  
  <body style="">
    <%@include file="menu.jsp" %>
      
<!-------------------------------------------HOME---------------------------------------------------->      
    <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal">
                <div class="container tarjetaLogueo">
                    <div class="d-flex justify-content-center h-100">
                            <div class="card ">
                                    <div class="card-header">
                                        <h3>Iniciar Sesión</h3>
                                    </div>
                                    <div class="card-body">
                                            <form method="POST" action="/RedSocialVecinal/Login">
                                                    <div class="input-group form-group">
                                                            <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                                            </div>
                                                        
                                                            <input type="text" id="txtNombre" class="form-control" name="txtNombre" aria-describedby="emailHelp" placeholder="Nombre de Usuario"/>

                                                    </div>
                                                    <div class="input-group form-group">
                                                            <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                                            </div>
                                                    
                                                            <input type="password" id="txtPass" class="form-control " name="txtPass" placeholder="Contraseña" /> 
                                                    </div>
                                                   
                                                    <div class="form-group">
                                                            <input type="submit" value="Login" class="btn float-right login_btn">
                                                    </div>
                                                    <div class="form-group">
                                                        <a href="/RedSocialVecinal/index.jsp" style="color: #fff" class="float-left"> Volver a Home</a>
                                                    </div>
                                            </form>
                                    </div>   
                            </div>
                    </div>
                </div> 
             </div>
    </div>
     
 <!-------------------------------------------FIN HOME------------------------------------------------>     
  <%@include file="footer.jsp" %>

</body>
</html>