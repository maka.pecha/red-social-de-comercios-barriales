<%-- 
    Document   : catalogoOfertas
    Created on : 9 nov. 2020, 13:58:39
    Author     : Maka
--%>
<%
    HttpSession sesion = request.getSession();
    Object usuario = sesion.getAttribute("usuario");

    if (usuario == null) {
        response.sendRedirect("/RedSocialVecinal/Login?accion=cerrar");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Catálogo Ofertas</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>
<!-------------------------------------------HOME---------------------------------------------------->      
        <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal ">

                <div class="secundarioCliente table-wrapper-scroll-y my-custom-scrollbar2">
                    <a href="/RedSocialVecinal/ListadoComercios"> <button class="btn btn-lg float-right" style="color: red">Volver a los Comercios</button></a>
                    <!--<a href="/RedSocialVecinal/CatalogoComercios" style="color: red" class="float-right"> Volver a Comercios</a>-->
                    <h2 style="padding-top: 2%; padding-left: 5%; color: white">Comentarios de ${nombre_comercio}</h2> 
                    <c:if test="${listaCo == []}">
                        <h4 id="sinComentarios" style="padding-top: 5%; color: white; display: flex;justify-content: center;">Este comercio aún no tiene comentarios</h2>
                    </c:if>
                    <c:if test="${listaCo != []}"> 
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Mensaje</th>
                            <th scope="col">Estrellas</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Respuesta</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listaCo}" var="c">      
                                <tr>
                                  <td>${c.getId()}</td>
                                  <td>${c.getTexto()}</td>
                                  <td>${c.getPuntaje()}</td>
                                  <td>${c.getVecino()}</td>
                                  <td>${c.getFecha()}</td>
                                  <td>${c.getRespuesta()}</td>
                                  <td><a href="/RedSocialVecinal/EliminarComentario?id=${id_comercio}&name=${nombre_comercio}&idComentario=${c.getId()}"><button type="button" class="btn btn-outline-danger">Eliminar</button></a></td>
                                  <c:if test="${c.getRespuesta() == null}">
                                    <td><a href="/RedSocialVecinal/ModerarComentariosPorComercio?accion=respuesta&id=${id_comercio}&name=${nombre_comercio}&id_comentario=${c.getId()}"><button type="button" class="btn btn-outline-info">Responder</button></a></td>
                                  </c:if>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    </c:if>
                    <c:if test="${id_comentario != null}">
                        <%@include file="agregarRespuesta.jsp" %>
                    </c:if>
                    
                </div>
            </div>
    </div>
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
    
</body>
</html>