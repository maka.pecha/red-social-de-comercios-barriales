<%-- 
    Document   : agregarRespuesta
    Created on : 19 nov. 2020, 12:32:34
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <body>
        <form  method="POST" action="/RedSocialVecinal/AgregarRespuesta?idComercio=${id_comercio}">
            <input hidden="" type="number" name="txtId_comentario" value="${id_comentario}"  class="form-control" id="txtId_comentario">
            <input hidden="" type="text" name="txtNombre_comercio" value="${nombre_comercio}" class="form-control" id="txtNombre_comercio">
            <div class="form-group">
              <label for="txtMensaje">RESPUESTA PARA EL COMENTARIO ${id_comentario}</label>
              <input name="txtMensaje" type="text" id="txtMensaje" required autofocus placeholder="Ingrese el Mensaje" class="form-control"/>
            </div>
            <input type="submit" class="btn btn-primary float-right" style="margin: 0 10px" value="Agregar"></button>
            <a href="/RedSocialVecinal/ModerarComentariosPorComercio?id=${id_comercio}&name=${nombre_comercio}"><button type="button" class="btn btn-danger float-right">Cancelar</button></a>
        </form>
    </body>
</html>
