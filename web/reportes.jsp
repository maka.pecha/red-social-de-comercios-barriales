<%-- 
    Document   : reportes
    Created on : 19 nov. 2020, 16:32:24
    Author     : Maka
--%>
<%
    HttpSession sesion = request.getSession();
    Object usuario = sesion.getAttribute("usuario");

    if (usuario == null) {
        response.sendRedirect("/RedSocialVecinal/Login?accion=cerrar");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Comercios</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>
      
<!-------------------------------------------HOME---------------------------------------------------->      
        <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal ">

                <div class="secundarioCliente table-wrapper-scroll-y my-custom-scrollbar2">
                    <h2 style="padding-left: 3%; color: white">Comercios ordenados por cantidad de comentarios</h2>
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">Comercio</th>
                            <th scope="col">Cantidad de comentarios</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${reporte1}" var="r1">      
                                <tr>
                                  <td>${r1.getComercio()}</td>
                                  <td>${r1.getCantidad()}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <h2 style="padding-left: 3%; color: white">Comentarios sin responder 📩</h2>
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">Mensaje</th>
                            <th scope="col">Estrellas</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Fecha</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${reporte2}" var="r2">      
                                <tr>
                                  <td>${r2.getTexto()}</td>
                                  <td>${r2.getPuntaje()}</td>
                                  <td>${r2.getVecino()}</td>
                                  <td>${r2.getFecha()}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <h2 style="padding-left: 3%; color: white">Promedio de valoración de cada comercio ➗</h2>
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">Comercio</th>
                            <th scope="col">Promedio de valoraciones</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${reporte3}" var="r3">      
                                <tr>
                                  <td>${r3.getComercio()}</td>
                                  <c:if test="${r3.getPromedio() != 0.0}"><td>${r3.getPromedio()}</td></c:if>
                                  <c:if test="${r3.getPromedio() == 0.0}"><td>Sin valoraciones</td></c:if>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <h2 style="padding-left: 3%; color: white">Cantidad de valoraciones por Estrellas ⭐</h2>
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">Estrellas</th>
                            <th scope="col">Cantidad de valoraciones</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${reporte4}" var="r4">      
                                <tr>
                                  <td>${r4.getEstrellas()}</td>
                                  <td>${r4.getValoraciones()}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>
