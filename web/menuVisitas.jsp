<%-- 
    Document   : menuVisitas
    Created on : 9 nov. 2020, 15:04:31
    Author     : Maka
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <header>
        <nav id="nav" class="navbar nav-transparent">
            <div class="container">
                <div class="navbar-header">
                    <div class="navbar-brand">
                        <a href="https://www.frc.utn.edu.ar/"> 
                            <img class="logo" src="Assets/Images/front/logoutn.png" alt="logo">
                            <img class="logo-alt" src="Assets/Images/front/logoutn2.png" alt="logo">
                        </a>
                    </div>
                    <div class="nav-collapse">
                        <span></span>
                    </div>
                 </div>
                <ul class="main-nav nav navbar-nav navbar-right tamañoMenu">
                    <li class="active"><a href="index.jsp">Home</a></li>
                    <li><a href="/RedSocialVecinal/CatalogoComercios">Comercios</a></li>
                    <li><a href="/RedSocialVecinal/BuscadorOfertas">Ofertas</a></li>
                </ul>
            </div>
        </nav>
    </header>
</html>
