/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Maka
 */
public class Comentario {
    private int id;
    private String texto;
    private int puntaje;
    private String vecino;
    private Comercio comercio;
    private String fecha;
    private boolean activo;

    public Comentario() {
    }

    public Comentario(int id, String texto, int puntaje, String vecino, Comercio comercio, String fecha, boolean activo) {
        this.id = id;
        this.texto = texto;
        this.puntaje = puntaje;
        this.vecino = vecino;
        this.comercio = comercio;
        this.fecha = fecha;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public String getTexto() {
        return texto;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public String getVecino() {
        return vecino;
    }

    public Comercio getComercio() {
        return comercio;
    }

    public String getFecha() {
        return fecha;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    public void setVecino(String vecino) {
        this.vecino = vecino;
    }

    public void setComercio(Comercio comercio) {
        this.comercio = comercio;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Comentario{" + "id=" + id + ", texto=" + texto + ", puntaje=" + puntaje + ", vecino=" + vecino + ", comercio=" + comercio + ", fecha=" + fecha + ", activo=" + activo + '}';
    }

}
