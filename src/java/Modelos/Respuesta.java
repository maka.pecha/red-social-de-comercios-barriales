/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Maka
 */
public class Respuesta {
    private int id;
    private String texto;
    private Comentario comentario;
    private String fecha;
    private boolean activo;

    public Respuesta() {
    }

    public Respuesta(int id, String texto, Comentario comentario, String fecha, boolean activo) {
        this.id = id;
        this.texto = texto;
        this.comentario = comentario;
        this.fecha = fecha;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public String getTexto() {
        return texto;
    }

    public Comentario getComentario() {
        return comentario;
    }

    public String getFecha() {
        return fecha;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public void setComentario(Comentario comentario) {
        this.comentario = comentario;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Respuesta{" + "id=" + id + ", texto=" + texto + ", comentario=" + comentario + ", fecha=" + fecha + ", activo=" + activo + '}';
    }
    
}
