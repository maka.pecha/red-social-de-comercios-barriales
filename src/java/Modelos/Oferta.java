/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;
import java.util.*;

/**
 *
 * @author Maka
 */
public class Oferta {
    private int id;
    private String titulo;
    private String descripcion;
    private String imagen;
    private float precio_real;
    private float precio_descuento;
    private Comercio comercio;
    private String fecha;
    private boolean activo;

    public Oferta(int id, String titulo, String descripcion, String imagen, float precio_real, float precio_descuento, Comercio comercio, String fecha, boolean activo) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.precio_real = precio_real;
        this.precio_descuento = precio_descuento;
        this.comercio = comercio;
        this.fecha = fecha;
        this.activo = activo;
    }

    public Oferta() {
    }

    public int getId() {
        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public float getPrecio_real() {
        return precio_real;
    }

    public float getPrecio_descuento() {
        return precio_descuento;
    }

    public Comercio getComercio() {
        return comercio;
    }

    public String getFecha() {
        return fecha;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setPrecio_real(float precio_real) {
        this.precio_real = precio_real;
    }

    public void setPrecio_descuento(float precio_descuento) {
        this.precio_descuento = precio_descuento;
    }

    public void setComercio(Comercio comercio) {
        this.comercio = comercio;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Oferta{" + "id=" + id + ", titulo=" + titulo + ", descripcion=" + descripcion + ", imagen=" + imagen + ", precio_real=" + precio_real + ", precio_descuento=" + precio_descuento + ", comercio=" + comercio + ", fecha=" + fecha + ", activo=" + activo + '}';
    }


}
