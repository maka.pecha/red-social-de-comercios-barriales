/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Maka
 */
public class Usuario {
    private int id;
    private String nombre;
    private String contraseña;
    private Comercio comercio;
    private boolean activo;

    public Usuario(int id, String nombre, String contraseña, Comercio comercio, boolean activo) {
        this.id = id;
        this.nombre = nombre;
        this.contraseña = contraseña;
        this.comercio = comercio;
        this.activo = activo;
    }

    public Usuario() {
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getContraseña() {
        return contraseña;
    }

    public Comercio getComercio() {
        return comercio;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setComercio(Comercio comercio) {
        this.comercio = comercio;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", nombre=" + nombre + ", contrase\u00f1a=" + contraseña + ", comercio=" + comercio + ", activo=" + activo + '}';
    }

}
