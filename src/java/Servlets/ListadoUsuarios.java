/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Gestores.*;
import Modelos.*;
import DTO.*;

/**
 *
 * @author Maka
 */
@WebServlet(name = "ListadoUsuarios", urlPatterns = {"/ListadoUsuarios"})
public class ListadoUsuarios extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                if (request.getSession().getAttribute("usuario") == null) {
            response.sendRedirect("index.jsp");
            return;
            }
        
        GestorUsuario gUsuario = new GestorUsuario();
        ArrayList<UsuarioDTO> lista = new ArrayList<>();
        lista = gUsuario.ObtenerTodosUsuarios();
        request.setAttribute("lista", lista);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/listadoUsuarios.jsp");
        rd.forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
