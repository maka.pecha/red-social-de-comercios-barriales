/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DTO.*;
import Gestores.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maka
 */
@WebServlet(name = "Reportes", urlPatterns = {"/Reportes"})
public class Reportes extends HttpServlet {
    GestorReportes gReportes = new GestorReportes();


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
            if (request.getSession().getAttribute("usuario") == null) {
                response.sendRedirect("index.jsp");
                return;
            }
        
        ArrayList<CantidadComentariosPorComercioDTO> reporte1 = new ArrayList<>();
        ArrayList<ComentarioDTO> reporte2 = new ArrayList<>();
        ArrayList<PromedioValoracionPorComercioDTO> reporte3 = new ArrayList<>();
        ArrayList<CantidadValidacionesPorEstrellasDTO> reporte4 = new ArrayList<>();
        reporte1 = gReportes.reporteCantidadComentariosPorComercio();
        reporte2 = gReportes.reporteComentariosNoRespondidos();
        reporte3 = gReportes.reportePromedioValoracionPorComercio();
        reporte4 = gReportes.reporteCantidadValoracionesPorEstrella();
        System.out.println(reporte1+" reporte 1");
        System.out.println(reporte2+" reporte 2");
        System.out.println(reporte3+" reporte 3");
        request.setAttribute("reporte1", reporte1);
        request.setAttribute("reporte2", reporte2);
        request.setAttribute("reporte3", reporte3);
        request.setAttribute("reporte4", reporte4);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/reportes.jsp");
        rd.forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
