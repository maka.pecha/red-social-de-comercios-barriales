/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DTO.*;
import Gestores.*;
import Modelos.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maka
 */
@WebServlet(name = "AgregarComentario", urlPatterns = {"/AgregarComentario"})
public class AgregarComentario extends HttpServlet {
    GestorComentario gComentario = new GestorComentario();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarComentario.jsp");
            rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String nombre_comercio = request.getParameter("txtNombre_comercio");
        
        int id_comentario = 0;
        String texto = request.getParameter("txtMensaje");
        int puntaje = Integer.parseInt(request.getParameter("txtEstrellas"));
        String vecino = request.getParameter("txtVecino");
        int id_comercio = Integer.parseInt(request.getParameter("txtId_comercio"));
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = sdf.format(new Date());
        boolean activo = true;
        Comercio comercio = new Comercio();
        comercio.setId(id_comercio);
        
        Comentario comentario = new Comentario(id_comentario, texto, puntaje, vecino, comercio, fecha, activo);
        System.out.println(comentario+" comentario para cargar");
        gComentario.agregarComentario(comentario);
        response.sendRedirect("/RedSocialVecinal/CatalogoOfertasPorComercio?id="+id_comercio+"&name="+nombre_comercio);        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
