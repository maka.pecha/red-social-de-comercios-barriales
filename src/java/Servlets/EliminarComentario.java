/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Gestores.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maka
 */
@WebServlet(name = "EliminarComentario", urlPatterns = {"/EliminarComentario"})
public class EliminarComentario extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("usuario") == null) {
            response.sendRedirect("index.jsp");
            return;
            }
        int id_comercio = Integer.parseInt(request.getParameter("id"));
        int id_comentario = Integer.parseInt(request.getParameter("idComentario"));
        String nombre_comercio = request.getParameter("name");
        System.out.println(id_comercio+" id_comercio "+id_comentario+" id_comentario "+nombre_comercio+" nombre_comercio");
        GestorComentario gestor = new GestorComentario();
        gestor.eliminarComentario(id_comentario);
        response.sendRedirect("/RedSocialVecinal/ModerarComentariosPorComercio?id="+id_comercio+"&name="+nombre_comercio);  
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
