/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DTO.*;
import Gestores.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maka
 */
@WebServlet(name = "BuscadorOfertas", urlPatterns = {"/BuscadorOfertas"})
public class BuscadorOfertas extends HttpServlet {

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        GestorOferta gOferta = new GestorOferta();
        ArrayList<OfertaDTO> lista = new ArrayList<>();
        lista = gOferta.ObtenerTodasOfertas();
        request.setAttribute("listaO", lista);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/buscadorOfertas.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String descripcion = request.getParameter("txtDescripcion");
        System.out.println(descripcion+" descripcion");
        GestorOferta gOferta = new GestorOferta();
        ArrayList<OfertaDTO> lista = new ArrayList<>();
        System.out.println(lista+" el resultado");
        lista = gOferta.ObtenerOfertasPorBusqueda(descripcion);
        request.setAttribute("listaO", lista);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/buscadorOfertas.jsp");
        rd.forward(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
