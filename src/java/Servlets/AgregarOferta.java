/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Gestores.*;
import Modelos.*;
import DTO.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Maka
 */
@WebServlet(name = "AgregarOferta", urlPatterns = {"/AgregarOferta"})
//para la imagen
@MultipartConfig(fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5,
        maxRequestSize = 1024 * 1024 * 5 * 5,
        location = "C:\\Users\\Maka.DESKTOP-E0EPKD2\\Documents\\UTN - Programación\\Laboratorio de Computacion IV\\RedSocialVecinal\\web\\Assets\\Images")
public class AgregarOferta extends HttpServlet {
    
    GestorOferta gOferta = new GestorOferta();
    GestorComercio gComercio = new GestorComercio();
    String nombreimagen = "";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
            if (request.getSession().getAttribute("usuario") == null) {
            response.sendRedirect("index.jsp");
            return;
        }
        ArrayList<ComercioDTO> lista = new ArrayList<>();
        lista = gComercio.ObtenerTodosComercios();
        request.setAttribute("listaC", lista);
         if (request.getParameter("id") == null) {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarOferta.jsp");
            rd.forward(request, response);
        } else {
            int id = Integer.parseInt(request.getParameter("id"));
            System.out.println(id+" id de oferta a editar");
            Oferta o = gOferta.ObtenerOfertaPorId(id);
            nombreimagen=o.getImagen();
            System.out.println(o+"oferta para editar");
            request.setAttribute("id", o.getId());
            request.setAttribute("titulo", o.getTitulo());
            request.setAttribute("descripcion", o.getDescripcion());
            request.setAttribute("imagen", o.getImagen());
            request.setAttribute("precio_real", o.getPrecio_real());
            request.setAttribute("precio_descuento", o.getPrecio_descuento());
            request.setAttribute("idComercioSelected", o.getComercio().getId());
            request.setAttribute("fecha", o.getFecha());
            request.setAttribute("activo", true);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarOferta.jsp");
            rd.forward(request, response);
        }    

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
            if (request.getSession().getAttribute("usuario") == null) {
            response.sendRedirect("index.jsp");
            return;
        }
        
        int id;
        boolean activo = true;
        Comercio comercio = new Comercio();
        if (request.getParameter("txtId").equals("")) {
            id = 0;
        } else {
            id = Integer.parseInt(request.getParameter("txtId"));
        }
        String titulo = request.getParameter("txtTitulo");
        String descripcion = request.getParameter("txtDescripcion");
        String imagen = request.getParameter("txtFoto");
        float precio_real = Float.parseFloat(request.getParameter("txtPrecio_real"));
        float precio_descuento = Float.parseFloat(request.getParameter("txtPrecio_descuento"));
        int idComercio = Integer.parseInt(request.getParameter("txtComercio"));
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = sdf.format(new Date());
        System.out.println(fecha+" fecha");
        comercio.setId(idComercio);
        
        //PARA LA IMAGEN 
        String nombreArchivo = "";
        for (Part part : request.getParts()) {
            String fileName = getFileName(part);
            if (!fileName.isEmpty()){ //Si la parte es un archivo
                part.write(fileName);// el parametro es el nombre NUEVO
                nombreArchivo = fileName;
            }
        }
        System.out.println(nombreArchivo+" nombreArchivo");
        if(nombreArchivo.isEmpty()){//si no se cambia la imagen que conserve la anterior
            nombreArchivo=nombreimagen;
        }

        Oferta o = new Oferta(id, titulo, descripcion, nombreArchivo, precio_real, precio_descuento, comercio, fecha, activo);
        
        System.out.println(o+" ofertaaa");
        if(id == 0){
            gOferta.agregarOferta(o);
        }
        else {
            if (gOferta.editarOferta(o)) {
                System.out.println("Se EDITÓ correctamente"); 
            }
            else{System.out.println("NO Se EDITÓ correctamente"); }
        }
        response.sendRedirect("/RedSocialVecinal/ListadoOfertas");
    }
    
    private String getFileName(Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf("=") + 2, content.length() - 1);
            }
        }
        return "";
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
