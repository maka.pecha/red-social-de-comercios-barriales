/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Gestores.*;
import Modelos.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maka
 */
@WebServlet(name = "AgregarRespuesta", urlPatterns = {"/AgregarRespuesta"})
public class AgregarRespuesta extends HttpServlet {
    GestorRespuesta gRespuesta = new GestorRespuesta();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
            int id = 0;
            int id_comentario = Integer.parseInt(request.getParameter("txtId_comentario"));
            String nombre_comercio = request.getParameter("txtNombre_comercio");
            String texto = request.getParameter("txtMensaje");
            int id_comercio = Integer.parseInt(request.getParameter("idComercio"));
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String fecha = sdf.format(new Date());
            boolean activo = true;
            Comentario comentario = new Comentario();
            comentario.setId(id_comentario);

            Respuesta respuesta = new Respuesta(id, texto, comentario, fecha, activo);
            System.out.println(respuesta+" respuesta para cargar");
            gRespuesta.agregarRespuesta(respuesta);
            response.sendRedirect("/RedSocialVecinal/ModerarComentariosPorComercio?id="+id_comercio+"&name="+nombre_comercio);     
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
