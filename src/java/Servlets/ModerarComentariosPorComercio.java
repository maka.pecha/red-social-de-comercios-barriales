/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DTO.*;
import Gestores.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maka
 */
@WebServlet(name = "ModerarComentariosPorComercio", urlPatterns = {"/ModerarComentariosPorComercio"})
public class ModerarComentariosPorComercio extends HttpServlet {
    GestorOferta gOferta = new GestorOferta();
    GestorComentario gComentario = new GestorComentario();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        if (request.getSession().getAttribute("usuario") == null) {
            response.sendRedirect("index.jsp");
            return;
            }
        int id = Integer.parseInt(request.getParameter("id"));
        String nombreComercio = request.getParameter("name");
        
        ArrayList<OfertaDTO> lista = new ArrayList<>();
        ArrayList<ComentarioDTO> listaCom = new ArrayList<>();
        System.out.println(id+" id de comercio para mostrar ofertas");
        lista = gOferta.ObtenerOfertasPorComercio(id);
        listaCom = gComentario.ObtenerComentariosPorIdComercio(id);
        
        ArrayList<EstrellaDTO> estrellas = new ArrayList<>();
        EstrellaDTO es = new EstrellaDTO(1, "⭐");
        estrellas.add(es);
        
        EstrellaDTO es2 = new EstrellaDTO(2, "⭐⭐");
        estrellas.add(es2);
        
        EstrellaDTO es3 = new EstrellaDTO(3, "⭐⭐⭐");
        estrellas.add(es3);
        
        EstrellaDTO es4 = new EstrellaDTO(4, "⭐⭐⭐⭐");
        estrellas.add(es4);
        
        EstrellaDTO es5 = new EstrellaDTO(5, "⭐⭐⭐⭐⭐");
        estrellas.add(es5);
        
        System.out.println(estrellas+" la lista de estrellas");
        request.setAttribute("listaEstrellas", estrellas);
        
        String accion= request.getParameter("accion");
                
        if(accion != null && accion.equals("respuesta")){
            int id_comentario = Integer.parseInt(request.getParameter("id_comentario"));
            System.out.println(id_comentario+" id_comentariooooooooo");
            request.setAttribute("id_comentario", id_comentario);
        }

        request.setAttribute("listaO", lista);
        request.setAttribute("listaCo", listaCom);
        request.setAttribute("nombre_comercio", nombreComercio);
        request.setAttribute("id_comercio", id);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/moderarComentariosPorComercio.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
