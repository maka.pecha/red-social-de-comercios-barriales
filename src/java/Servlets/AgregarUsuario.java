/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DTO.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Gestores.*;
import Modelos.*;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
/**
 *
 * @author Maka
 */
@WebServlet(name = "AgregarUsuario", urlPatterns = {"/AgregarUsuario"})
public class AgregarUsuario extends HttpServlet {
    GestorUsuario gUsuario = new GestorUsuario();
    GestorComercio gComercio = new GestorComercio();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                if (request.getSession().getAttribute("usuario") == null) {
            response.sendRedirect("index.jsp");
            return;
            }
        ArrayList<ComercioDTO> lista = new ArrayList<>();
        lista = gComercio.ObtenerTodosComercios();
        request.setAttribute("listaC", lista);
         if (request.getParameter("id") == null) {

            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarUsuario.jsp");
            rd.forward(request, response);
        } else {
            int id = Integer.parseInt(request.getParameter("id"));
            System.out.println(id+" id de usuario a editar");
            Usuario u = gUsuario.ObtenerUsuarioPorId(id);
            System.out.println(u+"usuario para editar");
            request.setAttribute("id", u.getId());
            request.setAttribute("nombre", u.getNombre());
            request.setAttribute("contraseña", u.getContraseña());
            request.setAttribute("idComercioSelected", u.getComercio().getId());
            request.setAttribute("activo", true);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarUsuario.jsp");
            rd.forward(request, response);
        }    

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
            if (request.getSession().getAttribute("usuario") == null) {
            response.sendRedirect("index.jsp");
            return;
        }
        
        int id;
        boolean activo = true;
        Comercio comercio = new Comercio();
        if (request.getParameter("txtId").equals("")) {
            id = 0;
        } else {
            id = Integer.parseInt(request.getParameter("txtId"));
        }
        String nombre = request.getParameter("txtNombre");
        String contraseña = request.getParameter("txtPass");
        int idComercio = Integer.parseInt(request.getParameter("txtComercio"));
        comercio.setId(idComercio);

        Usuario u = new Usuario(id, nombre, contraseña, comercio, activo);
        if(id == 0){
            gUsuario.agregarUsuario(u);
        }
        else {
            if (gUsuario.editarUsuario(u)) {
                System.out.println("Se EDITÓ correctamente"); 
            }
            else{System.out.println("NO Se EDITÓ correctamente"); }
        }
        response.sendRedirect("/RedSocialVecinal/ListadoUsuarios");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
