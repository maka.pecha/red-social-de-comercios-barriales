/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DTO.*;
import Gestores.*;
import Modelos.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Maka
 */

@WebServlet(name = "AgregarComercio", urlPatterns = {"/AgregarComercio"})
//para la imagen
@MultipartConfig(fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5,
        maxRequestSize = 1024 * 1024 * 5 * 5,
        location = "C:\\Users\\Maka.DESKTOP-E0EPKD2\\Documents\\UTN - Programación\\Laboratorio de Computacion IV\\RedSocialVecinal\\web\\Assets\\Images")

public class AgregarComercio extends HttpServlet {
    GestorComercio gComercio = new GestorComercio();
    GestorRubro gRubro = new GestorRubro();
    String nombreimagen = "";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
            if (request.getSession().getAttribute("usuario") == null) {
            response.sendRedirect("index.jsp");
            return;
        }
        ArrayList<Rubro> lista = new ArrayList<>();
        lista = gRubro.ObtenerTodosRubros();
        request.setAttribute("listaRubros", lista);
         if (request.getParameter("id") == null) {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarComercio.jsp");
            rd.forward(request, response);
        } else {
            int id = Integer.parseInt(request.getParameter("id"));
            System.out.println(id+" id de comercio a editar");
            Comercio c = gComercio.ObtenerComercioPorId(id);
            nombreimagen=c.getImagen();
            System.out.println(c+"comercio para editar");
            request.setAttribute("id", c.getId());
            request.setAttribute("nombre", c.getNombre());
            request.setAttribute("descripcion", c.getDescripcion());
            request.setAttribute("imagen", c.getImagen());
            request.setAttribute("idRubroSelected", c.getRubro().getId());
            request.setAttribute("activo", true);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarComercio.jsp");
            rd.forward(request, response);
        }    

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
            if (request.getSession().getAttribute("usuario") == null) {
            response.sendRedirect("index.jsp");
            return;
        }
        
        int id;
        boolean activo = true;
        Rubro rubro = new Rubro();
        if (request.getParameter("txtId").equals("")) {
            id = 0;
        } else {
            id = Integer.parseInt(request.getParameter("txtId"));
        }
        String nombre = request.getParameter("txtNombre");
        String descripcion = request.getParameter("txtDescripcion");
        String imagen = request.getParameter("txtFoto");
        int idRubro = Integer.parseInt(request.getParameter("txtRubro"));
        rubro.setId(idRubro);
        
        //PARA LA IMAGEN 
        String nombreArchivo = "";
        for (Part part : request.getParts()) {
            String fileName = getFileName(part);
            if (!fileName.isEmpty()){ //Si la parte es un archivo
                part.write(fileName);// el parametro es el nombre NUEVO
                nombreArchivo = fileName;
            }
        }
        System.out.println(nombreArchivo+" nombreArchivo");
        if(nombreArchivo.isEmpty()){//si no se cambia la imagen que conserve la anterior
            nombreArchivo=nombreimagen;
        }

        Comercio c = new Comercio(id, nombre, descripcion, nombreArchivo, rubro, activo);
        if(id == 0){
            gComercio.agregarComercio(c);
        }
        else {
            if (gComercio.editarComercio(c)) {
                System.out.println("Se EDITÓ correctamente"); 
            }
            else{System.out.println("NO Se EDITÓ correctamente"); }
        }
        response.sendRedirect("/RedSocialVecinal/ListadoComercios");
    }
    
    private String getFileName(Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf("=") + 2, content.length() - 1);
            }
        }
        return "";
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
