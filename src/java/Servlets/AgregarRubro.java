/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Gestores.*;
import Modelos.*;
import DTO.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maka
 */
@WebServlet(name = "AgregarRubro", urlPatterns = {"/AgregarRubro"})
public class AgregarRubro extends HttpServlet {
    GestorRubro gRubro = new GestorRubro();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        if (request.getSession().getAttribute("usuario") == null) {
            response.sendRedirect("index.jsp");
            return;
        }

        if (request.getParameter("id") == null) {

            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarRubro.jsp");
            rd.forward(request, response);
        } else {
            int id = Integer.parseInt(request.getParameter("id"));
            System.out.println(id+" id de usuario a editar");
            Rubro r = gRubro.ObtenerRubroPorId(id);
            System.out.println(r+"rubro para editar");
            request.setAttribute("id", r.getId());
            request.setAttribute("nombre", r.getNombre());
            request.setAttribute("descripcion", r.getDescripcion());
            request.setAttribute("activo", true);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarRubro.jsp");
            rd.forward(request, response);
        }    

    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
            if (request.getSession().getAttribute("usuario") == null) {
            response.sendRedirect("index.jsp");
            return;
        }
        
        int id;
        boolean activo = true;
        if (request.getParameter("txtId").equals("")) {
            id = 0;
        } else {
            id = Integer.parseInt(request.getParameter("txtId"));
        }
        String nombre = request.getParameter("txtNombre");
        String descripcion = request.getParameter("txtDescripcion");

        Rubro r = new Rubro(id, nombre, descripcion, activo);
        if(id == 0){
            gRubro.agregarRubro(r);
        }
        else {
            if (gRubro.editarRubro(r)) {
                System.out.println("Se EDITÓ correctamente"); 
            }
            else{System.out.println("NO Se EDITÓ correctamente"); }
        }
        response.sendRedirect("/RedSocialVecinal/ListadoRubros");
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
