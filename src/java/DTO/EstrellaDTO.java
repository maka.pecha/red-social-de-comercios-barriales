/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Maka
 */
public class EstrellaDTO {
    private int numero;
    private String texto;

    public EstrellaDTO() {
    }

    public EstrellaDTO(int numero, String texto) {
        this.numero = numero;
        this.texto = texto;
    }

    public int getNumero() {
        return numero;
    }

    public String getTexto() {
        return texto;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    @Override
    public String toString() {
        return "EstrellaDTO{" + "numero=" + numero + ", texto=" + texto + '}';
    }
    
}
