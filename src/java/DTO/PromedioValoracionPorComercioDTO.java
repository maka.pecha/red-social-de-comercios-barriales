/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Maka
 */
public class PromedioValoracionPorComercioDTO {
    private String comercio;
    private float promedio;

    public PromedioValoracionPorComercioDTO() {
    }

    public PromedioValoracionPorComercioDTO(String comercio, float promedio) {
        this.comercio = comercio;
        this.promedio = promedio;
    }

    public String getComercio() {
        return comercio;
    }

    public float getPromedio() {
        return promedio;
    }

    public void setComercio(String comercio) {
        this.comercio = comercio;
    }

    public void setPromedio(float promedio) {
        this.promedio = promedio;
    }

    @Override
    public String toString() {
        return "promedioValoracionPorComercioDTO{" + "comercio=" + comercio + ", promedio=" + promedio + '}';
    }
    
}
