/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Maka
 */
public class CantidadComentariosPorComercioDTO {
    private String comercio;
    private int cantidad;

    public CantidadComentariosPorComercioDTO() {
    }

    public CantidadComentariosPorComercioDTO(String comercio, int cantidad) {
        this.comercio = comercio;
        this.cantidad = cantidad;
    }

    public String getComercio() {
        return comercio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setComercio(String comercio) {
        this.comercio = comercio;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "cantidadComentariosPorComercioDTO{" + "comercio=" + comercio + ", cantidad=" + cantidad + '}';
    }
    
}
