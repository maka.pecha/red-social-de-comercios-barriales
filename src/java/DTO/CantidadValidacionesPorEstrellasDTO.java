/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Maka
 */
public class CantidadValidacionesPorEstrellasDTO {
    private int estrellas;
    private int valoraciones;

    public CantidadValidacionesPorEstrellasDTO() {
    }

    public CantidadValidacionesPorEstrellasDTO(int estrellas, int valoraciones) {
        this.estrellas = estrellas;
        this.valoraciones = valoraciones;
    }

    public int getEstrellas() {
        return estrellas;
    }

    public int getValoraciones() {
        return valoraciones;
    }

    public void setEstrellas(int estrellas) {
        this.estrellas = estrellas;
    }

    public void setValoraciones(int valoraciones) {
        this.valoraciones = valoraciones;
    }

    @Override
    public String toString() {
        return "CantidadValidacionesPorEstrellasDTO{" + "estrellas=" + estrellas + ", valoraciones=" + valoraciones + '}';
    }
    
}
