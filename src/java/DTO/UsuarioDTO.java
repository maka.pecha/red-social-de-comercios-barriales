/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Maka
 */
public class UsuarioDTO {
    private int id;
    private String nombre;
    private String comercio;
    private boolean activo;

    public UsuarioDTO() {
    }

    public UsuarioDTO(int id, String nombre, String comercio, boolean activo) {
        this.id = id;
        this.nombre = nombre;
        this.comercio = comercio;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getComercio() {
        return comercio;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setComercio(String comercio) {
        this.comercio = comercio;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "UsuarioDTO{" + "id=" + id + ", nombre=" + nombre + ", comercio=" + comercio + ", activo=" + activo + '}';
    }

}
