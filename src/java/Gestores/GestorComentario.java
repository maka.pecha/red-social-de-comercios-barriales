/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gestores;

import DTO.ComentarioDTO;
import DTO.*;
import Modelos.*;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Maka
 */
public class GestorComentario {
    ConexionDB con = new ConexionDB();
    
    public ArrayList<ComentarioDTO> ObtenerTodosComentarios(){
        ArrayList<ComentarioDTO> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM obtenerComentarios");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String texto = rs.getString("texto");
                int puntaje = rs.getInt("puntaje");
                String vecino = rs.getString("vecino");
                String comercio = rs.getString("comercio");
                String fecha = rs.getString("fecha");
                String respuesta = rs.getString("respuesta");
                boolean activo = rs.getBoolean("activo");
                ComentarioDTO c = new ComentarioDTO(id, texto, puntaje, vecino, comercio, fecha, respuesta, activo);
                lista.add(c);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return lista;
    }
    
    public ArrayList<ComentarioDTO> ObtenerComentariosPorIdComercio(int idComercio){
        ArrayList<ComentarioDTO> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM obtenerComentariosPorIdComercio("+idComercio+")");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String texto = rs.getString("texto");
                int puntaje = rs.getInt("puntaje");
                String vecino = rs.getString("vecino");
                String comercio = rs.getString("comercio");
                String fecha = rs.getString("fecha");
                String respuesta = rs.getString("respuesta");
                boolean activo = rs.getBoolean("activo");
                ComentarioDTO c = new ComentarioDTO(id, texto, puntaje, vecino, comercio, fecha, respuesta, activo);
                lista.add(c);

            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        System.out.println(lista+" comentarios");
        return lista;
    }
    
    public void agregarComentario(Comentario nuevo){
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call altaComentario (?,?,?,?,?,?)}");
            cst.setString(1, nuevo.getTexto());
            cst.setInt(2, nuevo.getPuntaje());
            cst.setString(3, nuevo.getVecino());
            cst.setInt(4, nuevo.getComercio().getId());
             cst.setString(5, nuevo.getFecha());
            cst.setBoolean(6, nuevo.isActivo());
            cst.execute();
            cst.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
    }
    
        public void eliminarComentario(int id){
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call bajaComentario (?)}");
            cst.setInt(1, id);
            cst.executeUpdate();
            cst.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
    }
}
