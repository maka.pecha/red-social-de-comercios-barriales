/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gestores;

import DTO.*;
import Modelos.*;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Maka
 */
public class GestorComercio {
    ConexionDB con = new ConexionDB();
            
    public ArrayList<ComercioDTO> ObtenerTodosComercios(){
        ArrayList<ComercioDTO> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM obtenerComercios");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String descripcion = rs.getString("descripcion");
                String imagen = rs.getString("imagen");
                String rubro = rs.getString("rubro");
                boolean activo = rs.getBoolean("activo");
                ComercioDTO c = new ComercioDTO(id, nombre, descripcion, imagen, rubro, activo);
                lista.add(c);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return lista;
    }
    
    public Comercio ObtenerComercioPorId(int idComercio){
        Comercio comercio = new Comercio();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM obtenerComercioPorId("+idComercio+")");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String descripcion = rs.getString("descripcion");
                String imagen = rs.getString("imagen");
                int id_rubro = rs.getInt("id_rubro");
                boolean activo = rs.getBoolean("activo");
                Rubro rubro = new Rubro();
                rubro.setId(id_rubro);
                comercio = new Comercio(id, nombre, descripcion, imagen, rubro, activo);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return comercio;
    }
    
    public void agregarComercio(Comercio nuevo){
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call altaComercio (?,?,?,?,?)}");
            cst.setString(1, nuevo.getNombre());
            cst.setString(2, nuevo.getDescripcion());
            cst.setString(3, nuevo.getImagen());
            cst.setInt(4, nuevo.getRubro().getId());
            cst.setBoolean(5, nuevo.isActivo());
            cst.execute();
            cst.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
    }
    
    public void eliminarComercio(int id){
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call bajaComercio (?)}");
            cst.setInt(1, id);
            cst.executeUpdate();
            cst.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
    }
    
    public Boolean editarComercio(Comercio editar){
        boolean flag= false;
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call modificarComercio (?,?,?,?,?,?)}");
            cst.setString(1, editar.getNombre());
            cst.setString(2, editar.getDescripcion());
            cst.setString(3, editar.getImagen());
            cst.setInt(4, editar.getRubro().getId());
            cst.setBoolean(5, editar.isActivo());
            cst.setInt(6, editar.getId());
            cst.executeUpdate();
            cst.close();
            flag= true;
        } catch (SQLException ex) {
            flag= false;
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return flag;
    }
}
