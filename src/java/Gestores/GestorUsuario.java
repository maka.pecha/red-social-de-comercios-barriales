/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gestores;

import Modelos.*;
import Gestores.*;
import DTO.*;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maka
 */
public class GestorUsuario {
    ConexionDB con = new ConexionDB();
            
    public ArrayList<UsuarioDTO> ObtenerTodosUsuarios(){
        ArrayList<UsuarioDTO> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM obtenerUsuariosComercio");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String comercio = rs.getString("comercio");
                boolean activo = rs.getBoolean("activo");
                if(comercio == null){
                    comercio= "Administrador";
                }
                UsuarioDTO u = new UsuarioDTO(id, nombre, comercio, activo);
                lista.add(u);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return lista;
    }
    
    public Usuario ObtenerUsuarioPorId(int idUsuario){
        Usuario usuario = new Usuario();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM obtenerUsuarioPorId("+idUsuario+")");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String contraseña = rs.getString("contraseña");
                int id_comercio = rs.getInt("id_comercio");
                boolean activo = rs.getBoolean("activo");
                Comercio comercio = new Comercio();
                comercio.setId(id_comercio);
                usuario = new Usuario(id, nombre, contraseña, comercio, activo);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return usuario;
    }
    
    public boolean ExisteUsuario(Usuario usuario) {
        boolean existe = false;
        try {
           con.conectar();
            String sql = "SELECT * FROM obtenerUsuarios WHERE nombre = ? AND contraseña = ?";
            PreparedStatement st = con.getConexion().prepareStatement(sql);
            
            st.setString(1, usuario.getNombre());
            st.setString(2, usuario.getContraseña());
            ResultSet rs = st.executeQuery();
            

            if ( rs.next() ) {
                existe = true;
            }
            rs.close();
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            con.desconectar();
        }
        return existe;
    }
    
    public void agregarUsuario(Usuario nuevo){
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call altaUsuario (?,?,?,?)}");
            cst.setString(1, nuevo.getNombre());
            cst.setString(2, nuevo.getContraseña());
            cst.setInt(3, nuevo.getComercio().getId());
            cst.setBoolean(4, nuevo.isActivo());
            cst.execute();
            cst.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
    }
    
    public void eliminarUsuario(int id){
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call bajaUsuario (?)}");
            cst.setInt(1, id);
            cst.executeUpdate();
            cst.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
    }
    
    public Boolean editarUsuario(Usuario editar){
        boolean flag= false;
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call modificarUsuario (?,?,?,?)}");
            cst.setString(1, editar.getNombre());
            cst.setString(2, editar.getContraseña());
            cst.setInt(3, editar.getComercio().getId());
            cst.setInt(4, editar.getId());
            cst.executeUpdate();
            cst.close();
            flag= true;
        } catch (SQLException ex) {
            flag= false;
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return flag;
    }

}
