/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gestores;

import DTO.*;
import Modelos.*;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 *
 * @author Maka
 */
public class GestorOferta {
    ConexionDB con = new ConexionDB();
            
    public ArrayList<OfertaDTO> ObtenerTodasOfertas(){
        ArrayList<OfertaDTO> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM obtenerOfertas");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String titulo = rs.getString("titulo");
                String descripcion = rs.getString("descripcion");
                String imagen = rs.getString("imagen");
                float precio_real = rs.getFloat("precio_real");
                float precio_descuento = rs.getFloat("precio_descuento");
                String comercio = rs.getString("comercio");
                String fecha = rs.getString("fecha");
                boolean activo = rs.getBoolean("activo");
                OfertaDTO o = new OfertaDTO(id, titulo, descripcion, imagen, precio_real, precio_descuento, comercio, fecha, activo);
                lista.add(o);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return lista;
    }
    
    public ArrayList<OfertaDTO> ObtenerOfertasPorComercio(int idComercio){
        ArrayList<OfertaDTO> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM obtenerOfertasPorComercio("+idComercio+")");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String titulo = rs.getString("titulo");
                String descripcion = rs.getString("descripcion");
                String imagen = rs.getString("imagen");
                float precio_real = rs.getFloat("precio_real");
                float precio_descuento = rs.getFloat("precio_descuento");
                String comercio = rs.getString("comercio");
                String fecha = rs.getString("fecha");
                boolean activo = rs.getBoolean("activo");
                OfertaDTO o = new OfertaDTO(id, titulo, descripcion, imagen, precio_real, precio_descuento, comercio, fecha, activo);
                lista.add(o);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return lista;
    }
    
    public ArrayList<OfertaDTO> ObtenerOfertasPorBusqueda(String busqueda){
        ArrayList<OfertaDTO> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
//            ResultSet rs = st.executeQuery("SELECT * FROM obtenerOfertasPorBusqueda("+busqueda+")");
            ResultSet rs = st.executeQuery("SELECT o.id, o.titulo, o.descripcion, o.imagen, o.precio_real, o.precio_descuento, c.nombre as comercio, o.fecha, o.activo\n" +
"				FROM Ofertas o join Comercios c on o.id_comercio = c.id\n" +
"				WHERE o.descripcion like '%"+busqueda+"%' OR o.titulo like '%"+busqueda+"%'\n" +
"				AND o.activo=1");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String titulo = rs.getString("titulo");
                String descripcion = rs.getString("descripcion");
                String imagen = rs.getString("imagen");
                float precio_real = rs.getFloat("precio_real");
                float precio_descuento = rs.getFloat("precio_descuento");
                String comercio = rs.getString("comercio");
                String fecha = rs.getString("fecha");
                boolean activo = rs.getBoolean("activo");
                OfertaDTO o = new OfertaDTO(id, titulo, descripcion, imagen, precio_real, precio_descuento, comercio, fecha, activo);
                lista.add(o);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return lista;
    }
    
    
    
    public Oferta ObtenerOfertaPorId(int idOferta){
        Oferta oferta = new Oferta();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM obtenerOfertaPorId("+idOferta+")");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String titulo = rs.getString("titulo");
                String descripcion = rs.getString("descripcion");
                String imagen = rs.getString("imagen");
                float precio_real = rs.getFloat("precio_real");
                float precio_descuento = rs.getFloat("precio_descuento");
                int id_comercio = rs.getInt("id_comercio");
                String fecha = rs.getString("fecha");
                boolean activo = rs.getBoolean("activo");
                Comercio comercio = new Comercio();
                comercio.setId(id_comercio);
                oferta = new Oferta(id, titulo, descripcion, imagen, precio_real, precio_descuento, comercio, fecha, activo);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return oferta;
    }
    
    public void agregarOferta(Oferta nuevo){
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call altaOferta (?,?,?,?,?,?,?,?)}");
            cst.setString(1, nuevo.getTitulo());
            cst.setString(2, nuevo.getDescripcion());
            cst.setString(3, nuevo.getImagen());
            cst.setFloat(4, nuevo.getPrecio_real());
            cst.setFloat(5, nuevo.getPrecio_descuento());
            cst.setInt(6, nuevo.getComercio().getId());
            cst.setString(7, nuevo.getFecha());
            cst.setBoolean(8, nuevo.isActivo());
            cst.execute();
            cst.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
    }
        
    public void eliminarOferta(int id){
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call bajaOferta (?)}");
            cst.setInt(1, id);
            cst.executeUpdate();
            cst.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
    }
    
    public Boolean editarOferta(Oferta editar){
        boolean flag= false;
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call modificarOferta (?,?,?,?,?,?,?,?,?)}");
            cst.setString(1, editar.getTitulo());
            cst.setString(2, editar.getDescripcion());
            cst.setString(3, editar.getImagen());
            cst.setFloat(4, editar.getPrecio_real());
            cst.setFloat(5, editar.getPrecio_descuento());
            cst.setInt(6, editar.getComercio().getId());
            cst.setString(7, editar.getFecha());
            cst.setBoolean(8, editar.isActivo());
            cst.setInt(9, editar.getId());
            cst.executeUpdate();
            cst.close();
            flag= true;
        } catch (SQLException ex) {
            flag= false;
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return flag;
    }
}
