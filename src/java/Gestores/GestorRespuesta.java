/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gestores;

import DTO.*;
import Modelos.*;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Maka
 */
public class GestorRespuesta {
    ConexionDB con = new ConexionDB();
    
    public void agregarRespuesta(Respuesta nueva){
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call altaRespuesta (?,?,?,?)}");
            cst.setString(1, nueva.getTexto());
            cst.setInt(2, nueva.getComentario().getId());
            cst.setString(3, nueva.getFecha());
            cst.setBoolean(4, nueva.isActivo());
            cst.execute();
            cst.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
    }
}
