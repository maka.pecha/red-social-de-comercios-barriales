/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gestores;

import DTO.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Maka
 */
public class GestorReportes {
    ConexionDB con = new ConexionDB();
    
    public ArrayList<CantidadComentariosPorComercioDTO> reporteCantidadComentariosPorComercio(){
        ArrayList<CantidadComentariosPorComercioDTO> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM reporteCantidadComentariosPorComercio order by cantidadComentarios desc");
          
            while (rs.next()) {
                String comercio = rs.getString("comercio");
                int cantidadComentarios = rs.getInt("cantidadComentarios");
                CantidadComentariosPorComercioDTO c = new CantidadComentariosPorComercioDTO(comercio, cantidadComentarios);
                lista.add(c);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return lista;
    }
    
    public ArrayList<ComentarioDTO> reporteComentariosNoRespondidos(){
        ArrayList<ComentarioDTO> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("select * from obtenerComentarios where respuesta is null");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String texto = rs.getString("texto");
                int puntaje = rs.getInt("puntaje");
                String vecino = rs.getString("vecino");
                String comercio = rs.getString("comercio");
                String fecha = rs.getString("fecha");
                String respuesta = rs.getString("respuesta");
                boolean activo = rs.getBoolean("activo");
                ComentarioDTO c = new ComentarioDTO(id, texto, puntaje, vecino, comercio, fecha, respuesta, activo);
                lista.add(c);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return lista;
    }
        
    public ArrayList<PromedioValoracionPorComercioDTO> reportePromedioValoracionPorComercio(){
        ArrayList<PromedioValoracionPorComercioDTO> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM reportePromedioValoracionPorComercio");
          
            while (rs.next()) {
                String comercio = rs.getString("comercio");
                float promedio = rs.getFloat("promedio");
                PromedioValoracionPorComercioDTO c = new PromedioValoracionPorComercioDTO(comercio, promedio);
                lista.add(c);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return lista;
    }
    
    public ArrayList<CantidadValidacionesPorEstrellasDTO> reporteCantidadValoracionesPorEstrella(){
        ArrayList<CantidadValidacionesPorEstrellasDTO> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM reporteCantidadValoracionesPorEstrella");
          
            while (rs.next()) {
                int estrellas = rs.getInt("estrellas");
                int cantidad = rs.getInt("cantidadValoraciones");
                CantidadValidacionesPorEstrellasDTO c = new CantidadValidacionesPorEstrellasDTO(estrellas, cantidad);
                lista.add(c);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return lista;
    }
}
