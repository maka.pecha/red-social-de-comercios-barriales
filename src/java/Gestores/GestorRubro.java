/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gestores;

import DTO.*;
import Modelos.*;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Maka
 */
public class GestorRubro {
    ConexionDB con = new ConexionDB();
            
    public ArrayList<Rubro> ObtenerTodosRubros(){
        ArrayList<Rubro> lista = new ArrayList<>();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM obtenerRubros");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String descripcion = rs.getString("descripcion");
                boolean activo = rs.getBoolean("activo");
                Rubro u = new Rubro(id, nombre, descripcion, activo);
                lista.add(u);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return lista;
    }
    
     public Rubro ObtenerRubroPorId(int idRubro){
        Rubro rubro = new Rubro();
        try {
            con.conectar();
            Statement st = con.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM obtenerRubroPorId("+idRubro+")");
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String descripcion = rs.getString("descripcion");
                boolean activo = rs.getBoolean("activo");
                rubro = new Rubro(id, nombre, descripcion, activo);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return rubro;
    }
     
    public void agregarRubro(Rubro nuevo){
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call altaRubro (?,?,?)}");
            cst.setString(1, nuevo.getNombre());
            cst.setString(2, nuevo.getDescripcion());
            cst.setBoolean(3, nuevo.isActivo());
            cst.execute();
            cst.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
    }
    
    public void eliminarRubro(int id){
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call bajaRubro (?)}");
            cst.setInt(1, id);
            cst.executeUpdate();
            cst.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
    }
    
    public Boolean editarRubro(Rubro editar){
        boolean flag= false;
        try {
            con.conectar();
            CallableStatement cst = con.getConexion().prepareCall("{call modificarRubro (?,?,?)}");
            cst.setString(1, editar.getNombre());
            cst.setString(2, editar.getDescripcion());
            cst.setInt(3, editar.getId());
            cst.executeUpdate();
            cst.close();
            flag= true;
        } catch (SQLException ex) {
            flag= false;
            ex.printStackTrace();
        }
        finally{
            con.desconectar();
        }
        return flag;
    }
}
